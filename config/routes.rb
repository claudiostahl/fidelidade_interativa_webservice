Rails.application.routes.draw do
  resources :cors
  
  get "/404" => "errors#not_found"
  get "/500" => "errors#exception"
  
  get 'shared/:token' => 'redirect#shared'
  
  namespace :api do
    scope '/v1' do
      scope '/premium' do
        get '/' => 'premium#index'
        post '/' => 'premium#create'
        get '/:id' => 'premium#view'
      end
      scope '/post' do
        get '/' => 'post#index'
        get '/:id' => 'post#view'
      end
      scope '/product' do
        get '/' => 'product#index'
        get '/:id' => 'product#view'
      end
      scope '/reward' do
        get '/' => 'reward#index'
        get '/:id' => 'reward#view'
      end
      scope '/user' do
        get '/' => 'user#index'
        get '/:id' => 'user#view'
      end
    end
  end
  
  namespace :frontend do
    scope '/v1' do
      post '/login' => 'connection#login'
      post '/logout' => 'connection#logout'
      post '/account' => 'account#update'
      scope '/recover' do
        post '/' => 'recover#create'
        put '/' => 'recover#update'
      end
      scope '/notification' do
        get '/' => 'notification#index'
        put '/' => 'notification#update'
        get '/:id' => 'notification#view'
        put '/:id' => 'notification#update'
      end
      scope '/order' do
        get '/' => 'order#index'
        post '/' => 'order#create'
        get '/:id' => 'order#view'
      end
      scope 'vote' do
        get '/:related' => 'vote#index'
        put '/:related' => 'vote#update'
      end
      scope '/access' do
        post '/' => 'access#create'
      end
      scope '/coupon_read' do
        post '/' => 'coupon_read#create'
      end
      scope '/shared' do
        post '/' => 'shared#create'
      end
      scope '/user' do
        post '/' => 'user#create'
      end
      scope '/cashflow' do
        get '/' => 'cashflow#index'
        get '/:id' => 'cashflow#view'
      end
      scope '/post' do
        get '/' => 'post#index'
        get '/:id' => 'post#view'
      end
      scope '/content' do
        get '/' => 'content#index'
      end
    end
  end

  namespace :backend do
    scope '/v1' do
      get '/ping' => 'ping#index'
      post '/ping' => 'ping#post'
      post '/login' => 'connection#login'
      post '/logout' => 'connection#logout'
      post '/account' => 'account#update'
      scope '/recover' do
        post '/' => 'recover#create'
        put '/' => 'recover#update'
      end
      scope '/administrator' do
        get '/' => 'administrator#index'
        post '/' => 'administrator#create'
        put '/' => 'administrator#update'
        delete '/' => 'administrator#destroy'
        get '/:id' => 'administrator#view'
        put '/:id' => 'administrator#update'
        delete '/:id' => 'administrator#destroy'
      end
      scope '/company' do
        get '/' => 'company#index'
        post '/' => 'company#create'
        put '/' => 'company#update'
        delete '/' => 'company#destroy'
        get '/:id' => 'company#view'
        put '/:id' => 'company#update'
        delete '/:id' => 'company#destroy'
      end
      scope '/developer' do
        get '/' => 'developer#index'
        post '/' => 'developer#create'
        put '/' => 'developer#update'
        delete '/' => 'developer#destroy'
        get '/:id' => 'developer#view'
        put '/:id' => 'developer#update'
        delete '/:id' => 'developer#destroy'
        put '/refresh-token/:id' => 'developer#refresh_token'
      end
      scope '/user' do
        get '/' => 'user#index'
        put '/' => 'user#update'
        get '/:id' => 'user#view'
        put '/:id' => 'user#update'
      end
      scope '/user_group' do
        get '/' => 'user_group#index'
        post '/' => 'user_group#create'
        put '/' => 'user_group#update'
        delete '/' => 'user_group#destroy'
        get '/:id' => 'user_group#view'
        put '/:id' => 'user_group#update'
        delete '/:id' => 'user_group#destroy'
      end
      scope '/product' do
        get '/' => 'product#index'
        post '/' => 'product#create'
        put '/' => 'product#update'
        delete '/' => 'product#destroy'
        get '/:id' => 'product#view'
        put '/:id' => 'product#update'
        delete '/:id' => 'product#destroy'
      end
      scope '/post' do
        get '/' => 'post#index'
        post '/' => 'post#create'
        put '/' => 'post#update'
        delete '/' => 'post#destroy'
        get '/:id' => 'post#view'
        put '/:id' => 'post#update'
        delete '/:id' => 'post#destroy'
      end
      scope '/reward' do
        get '/' => 'reward#index'
        post '/' => 'reward#create'
        put '/' => 'reward#update'
        delete '/' => 'reward#destroy'
        get '/:id' => 'reward#view'
        put '/:id' => 'reward#update'
        delete '/:id' => 'reward#destroy'
      end
      scope '/reward_rule' do
        get '/' => 'reward_rule#index'
        post '/' => 'reward_rule#create'
        put '/' => 'reward_rule#update'
        delete '/' => 'reward_rule#destroy'
        get '/:id' => 'reward_rule#view'
        put '/:id' => 'reward_rule#update'
        delete '/:id' => 'reward_rule#destroy'
      end
      scope '/message' do
        get '/' => 'message#index'
        post '/' => 'message#create'
        put '/' => 'message#update'
        delete '/' => 'message#destroy'
        get '/:id' => 'message#view'
        put '/:id' => 'message#update'
        delete '/:id' => 'message#destroy'
      end
      scope '/product_block' do
        get '/' => 'product_block#index'
        post '/' => 'product_block#create'
        put '/' => 'product_block#update'
        delete '/' => 'product_block#destroy'
        get '/:id' => 'product_block#view'
        put '/:id' => 'product_block#update'
        delete '/:id' => 'product_block#destroy'
      end
      scope '/reward_block' do
        get '/' => 'reward_block#index'
        post '/' => 'reward_block#create'
        put '/' => 'reward_block#update'
        delete '/' => 'reward_block#destroy'
        get '/:id' => 'reward_block#view'
        put '/:id' => 'reward_block#update'
        delete '/:id' => 'reward_block#destroy'
      end
      scope '/product_photo' do
        get '/' => 'product_photo#index'
        post '/' => 'product_photo#create'
        put '/' => 'product_photo#update'
        delete '/' => 'product_photo#destroy'
        get '/:id' => 'product_photo#view'
        put '/:id' => 'product_photo#update'
        delete '/:id' => 'product_photo#destroy'
      end
      scope '/reward_photo' do
        get '/' => 'reward_photo#index'
        post '/' => 'reward_photo#create'
        put '/' => 'reward_photo#update'
        delete '/' => 'reward_photo#destroy'
        get '/:id' => 'reward_photo#view'
        put '/:id' => 'reward_photo#update'
        delete '/:id' => 'reward_photo#destroy'
      end
      scope '/coupon' do
        get '/' => 'coupon#index'
        post '/' => 'coupon#create'
        put '/' => 'coupon#update'
        delete '/' => 'coupon#destroy'
        get '/:id' => 'coupon#view'
        put '/:id' => 'coupon#update'
        delete '/:id' => 'coupon#destroy'
      end
      scope '/coupon_group' do
        get '/' => 'coupon_group#index'
        post '/' => 'coupon_group#create'
        put '/' => 'coupon_group#update'
        delete '/' => 'coupon_group#destroy'
        get '/:id' => 'coupon_group#view'
        put '/:id' => 'coupon_group#update'
        delete '/:id' => 'coupon_group#destroy'
      end
      scope '/tutorial' do
        get '/' => 'tutorial#index'
        post '/' => 'tutorial#create'
        put '/' => 'tutorial#update'
        delete '/' => 'tutorial#destroy'
        get '/:id' => 'tutorial#view'
        put '/:id' => 'tutorial#update'
        delete '/:id' => 'tutorial#destroy'
      end
      scope '/menu' do
        get '/' => 'menu#index'
        post '/' => 'menu#create'
        put '/' => 'menu#update'
        delete '/' => 'menu#destroy'
        get '/:id' => 'menu#view'
        put '/:id' => 'menu#update'
        delete '/:id' => 'menu#destroy'
      end
      scope '/premium_type' do
        get '/' => 'premium_type#index'
        post '/' => 'premium_type#create'
        put '/' => 'premium_type#update'
        delete '/' => 'premium_type#destroy'
        get '/:id' => 'premium_type#view'
        put '/:id' => 'premium_type#update'
        delete '/:id' => 'premium_type#destroy'
      end
      scope '/banner' do
        get '/' => 'banner#index'
        post '/' => 'banner#create'
        put '/' => 'banner#update'
        delete '/' => 'banner#destroy'
        get '/:id' => 'banner#view'
        put '/:id' => 'banner#update'
        delete '/:id' => 'banner#destroy'
      end
      scope '/language' do
        get '/' => 'language#index'
        post '/' => 'language#create'
        put '/' => 'language#update'
        delete '/' => 'language#destroy'
        get '/:id' => 'language#view'
        put '/:id' => 'language#update'
        delete '/:id' => 'language#destroy'
      end
      scope '/order' do
        get '/' => 'order#index'
        post '/' => 'order#create'
        put '/' => 'order#update'
        get '/:id' => 'order#view'
        put '/:id' => 'order#update'
      end
      scope '/order_confirmation' do
        get '/' => 'order_confirmation#index'
        post '/' => 'order_confirmation#create'
        put '/' => 'order_confirmation#update'
        get '/:id' => 'order_confirmation#view'
        put '/:id' => 'order_confirmation#update'
      end
      scope '/order_refund' do
        get '/' => 'order_refund#index'
        post '/' => 'order_refund#create'
        put '/' => 'order_refund#update'
        get '/:id' => 'order_refund#view'
        put '/:id' => 'order_refund#update'
      end
      scope '/order_canceled' do
        get '/' => 'order_canceled#index'
        post '/' => 'order_canceled#create'
        put '/' => 'order_canceled#update'
        get '/:id' => 'order_canceled#view'
        put '/:id' => 'order_canceled#update'
      end
      scope 'config' do
        get '/' => 'config#index'
        put '/' => 'config#update'
      end
      scope 'translation' do
        get '/:related/:related_id' => 'translation#index'
        put '/:related/:related_id' => 'translation#update'
      end
      scope '/premium' do
        get '/' => 'premium#index'
        get '/:id' => 'premium#view'
      end
      scope '/cashflow' do
        get '/' => 'cashflow#index'
        get '/:id' => 'cashflow#view'
      end
      scope '/coupon_read' do
        get '/' => 'coupon_read#index'
        get '/:id' => 'coupon_read#view'
      end
      scope '/vote' do
        get '/' => 'vote#index'
        get '/:id' => 'vote#view'
      end
      scope '/notification' do
        get '/' => 'notification#index'
        get '/:id' => 'notification#view'
      end
      scope '/shared' do
        get '/' => 'shared#index'
        get '/:id' => 'shared#view'
      end
      scope '/shared_access' do
        get '/' => 'shared_access#index'
        get '/:id' => 'shared_access#view'
      end
      scope '/access' do
        get '/' => 'access#index'
        get '/:id' => 'access#view'
      end
      scope '/package' do
        get '/' => 'package#index'
      end
      scope '/resource' do
        get '/' => 'resource#index'
      end
    end
  end

end
