class Time
   
   def convert_to_second
       h = self.hour * 60 * 60
       m = self.min * 60
       s = self.sec
       h + m + s
   end
    
end