class ActiveRecord::Base

  def builder_json_by_array(attributes=[])
    attrs = as_json({})
    attrs_images = []
    c = Kernel.const_get self.class.name
    reflections = c.reflections.collect{|a, b| a}.compact
    
    if self.respond_to? :image_names
      for index in image_names
        images = {}
        for name in self.image_styles
          images[name.to_s] = self.image(name)
        end
        attrs[index.to_s] = images
        attrs_images << index.to_s
      end
    end
    
    for reflection in reflections
      if self.send(reflection).kind_of? ActiveRecord::Associations::CollectionProxy
        attrs[reflection] = []
        for m in self.send(reflection)
          if (m.respond_to?(:related_attribute))
            attrs[reflection] << m.related_attribute
          else
            attrs[reflection] << m.id
          end
        end
      elsif self.send(reflection).kind_of? ActiveRecord::Base
        if (self.send(reflection).respond_to?(:related_attribute))
          attrs[reflection] = self.send(reflection).related_attribute
        else
          attrs[reflection] = self.send(reflection).id
        end
      end
    end
    
    output = Hash.new
    if (attributes.count > 0)
      if attributes.is_a? Hash
        attributes.each do |key, value|
          if attrs.has_key?(value.to_s)
            output[key.to_s] = attrs[value.to_s]
            if attrs_images.include? value.to_s
              output[key.to_s + "_file_name"] = attrs[value.to_s + "_file_name"]
              output[key.to_s + "_content_type"] = attrs[value.to_s + "_content_type"]
              output[key.to_s + "_file_size"] = attrs[value.to_s + "_file_size"]
              output[key.to_s + "_updated_at"] = attrs[value.to_s + "_updated_at"]
            end
          end
        end
      else
        for attribute in attributes
          if attrs.has_key?(attribute.to_s)
            output[attribute.to_s] = attrs[attribute.to_s]
            if attrs_images.include? attribute.to_s
              output[attribute.to_s + "_file_name"] = attrs[attribute.to_s + "_file_name"]
              output[attribute.to_s + "_content_type"] = attrs[attribute.to_s + "_content_type"]
              output[attribute.to_s + "_file_size"] = attrs[attribute.to_s + "_file_size"]
              output[attribute.to_s + "_updated_at"] = attrs[attribute.to_s + "_updated_at"]
            end
          end
        end
      end
    else
      output = output.merge attrs
    end
    output
  end
  
  def get_custom_value
    if self.value_number != nil
      value = self.value_number
    elsif self.value_datetime != nil
      value = self.value_datetime
    elsif self.value_boolean != nil
      value = self.value_boolean
    elsif self.value_text != nil
      value = self.value_text
    else
      value = self.value_string
    end
    value
  end
  
  def populate_custom_value(value)
    self.value_number = nil
    self.value_datetime = nil
    self.value_boolean = nil
    self.value_text = nil
    self.value_string = nil
    if value != nil
      if value.is_a?(TrueClass) || value.is_a?(FalseClass)
        self.value_boolean = value
      elsif value.is_number?
        self.value_number = value
      elsif ((DateTime.parse(value) rescue ArgumentError) != ArgumentError)
        self.value_datetime = value
      elsif value.length > 200
        self.value_text = value
      else
        self.value_string = value
      end
    end
    self
  end

end