class Object

  def is_number?
    true if Float(self) rescue false
  end
  
  def is_integer?
    true if Integer(self) rescue false
  end

end