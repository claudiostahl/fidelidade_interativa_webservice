class ActiveSupport::TimeWithZone

    def as_json(options = nil)
       self.to_datetime.to_s
    end

end