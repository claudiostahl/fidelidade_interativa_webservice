require 'application_controller_test'

module ApplicationControllerTestAssert
  
  def assert_image(result, attribute)
    assert result.has_key?("success")
    assert_equal result["success"][attribute.to_s + "_file_name"], "photo.jpg"
    assert_equal result["success"][attribute.to_s + "_content_type"], "image/jpeg"
    assert_equal result["success"][attribute.to_s + "_file_size"], 645937
  end
  
  def assert_success(data, result, attribute = nil)
    message =  "REQUEST: " + data.to_s + "\n" + "RESPONSE: " + result.to_s
    if attribute != nil
      message += "\n" + "attribute: " + attribute.to_s
    end
    assert result.has_key?("success"), message
  end
  
  def assert_error_badrequest(data, result, attribute = nil)
    assert_error :bad_request, 400, data, result, attribute
  end
  
  def assert_error_unauthorized(data, result, attribute = nil)
    assert_error :unauthorized, 401, data, result, attribute
  end
  
  def assert_error_invalid(data, result, attribute = nil)
    assert_error :invalid, 402, data, result, attribute
  end
  
  def assert_error_notfound(data, result, attribute = nil)
    assert_error :not_found, 404, data, result, attribute
  end
  
  def assert_error(code, status, data, result, attribute)
    message =  "REQUEST: " + data.to_s + "\n" + "RESPONSE: " + result.to_s
    if attribute != nil
      message += "\n" + "attribute: " + attribute.to_s
    end
    assert result.has_key?("error"), message
    assert_equal result["error"]["code"], code.to_s, message
    assert_response status
  end
  
  def assert_find_many_data(data)
    for attribute in @attribute_string
      assert_find_data data, attribute
    end
    for attribute in @attribute_email
      assert_find_data data, attribute
    end
  end
  
  def assert_find_many_invalid_data(data)
    for attribute in @attribute_string
      assert_find_invalid_data data, attribute
    end
    for attribute in @attribute_email
      assert_find_invalid_data data, attribute
    end
  end
  
  def assert_find_data(data, attribute, attribute2=nil)
    value = data[attribute]
    if (!attribute2)
      entity = @model_class.where(attribute => value).first
    else
      value2 = data[attribute2]
      entity = @model_class.where(attribute => value, attribute2 => value2).first
    end
    assert entity, "attribute: " + attribute.to_s + "\n" + "value: " + value.to_s
    assert_equal entity.attributes[attribute], value, "attribute: " + attribute.to_s + "\n" + "value: " + value.to_s
  end
  
  def assert_find_invalid_data(data, attribute, attribute2=nil)
    value = data[attribute]
    if (!attribute2)
      entity = @model_class.where(attribute => value).first
    else
      value2 = data[attribute2]
      entity = @model_class.where(attribute => value, attribute2 => value2).first
    end
    assert_nil entity, "attribute: " + attribute.to_s + "\n" + "value: " + value.to_s
  end
  
end