require 'test_helper'
require 'application_controller_test_assert'
require 'application_controller_test_util'
require 'application_controller_test_default'
require 'application_controller_test_advanced'

class ApplicationControllerTest < ActionController::TestCase
  
  include ApplicationControllerTestAssert
  include ApplicationControllerTestUtil
  include ApplicationControllerTestDefault
  include ApplicationControllerTestAdvanced
  
  def setup
    @test_default = []
    @api_device = nil
    @api_email = nil
    @api_token = nil
    @api_password = nil
    @authenticate = {}
    @controller = nil
    @model_class = nil
    @related = nil
    @can_many = true
    @objects = []
    @attribute_string = []
    @attribute_small_string = []
    @attribute_email = []
    @attribute_url = []
    @attribute_boolean = []
    @attribute_integer = []
    @attribute_number = []
    @attribute_date = []
    @attribute_datetime = []
    @attribute_time = []
    @attribute_image = []
    @attribute_password = []
    @is_required = []
    @not_required = []
    @is_unique = []
    @is_unique_company = []
    @attribute_default = {}
    @max_lenght = {}
    @unallowed = {}
    @has_one = {}
    @has_many = {}
    init
    apply_config
  end
  
  def init
    
  end
  
  def apply_config
    @attribute_string = array_symbol_to_string @attribute_string
    @attribute_small_string = array_symbol_to_string @attribute_small_string
    @attribute_email = array_symbol_to_string @attribute_email
    @attribute_url = array_symbol_to_string @attribute_url
    @attribute_boolean = array_symbol_to_string @attribute_boolean
    @attribute_integer = array_symbol_to_string @attribute_integer
    @attribute_number = array_symbol_to_string @attribute_number
    @attribute_date = array_symbol_to_string @attribute_date
    @attribute_datetime = array_symbol_to_string @attribute_datetime
    @attribute_time = array_symbol_to_string @attribute_time
    @is_required = array_symbol_to_string @is_required
    @not_required = array_symbol_to_string @not_required
    @is_unique = array_symbol_to_string @is_unique
    @is_unique_company = array_symbol_to_string @is_unique_company
    @attribute_default = hash_symbol_to_string @attribute_default
    @max_lenght = hash_symbol_to_string @max_lenght
    @unallowed = hash_symbol_to_string @unallowed
    @has_one = hash_symbol_to_string @has_one
    @has_many = hash_symbol_to_string @has_many
    refresh_header
  end
  
  def config_backend
    @api_device = "device_root"
    @api_token = "token_root"
    @api_password = "102030"
    refresh_header
  end
  
  def config_backend_other_company
    @api_device = "device_other_company"
    @api_token = "token_other_company"
    @api_password = "102030"
    refresh_header
  end
  
  def config_frontend
    @api_device = "device_user"
    @api_token = "token_user"
    @api_password = "102030"
    refresh_header
  end
  
  def config_api
    @api_email = "developer1@teste.com"
    @api_token = "abcdefghijklmnopqrstuvxz1"
    refresh_header
  end
  
  def clear_header
    @api_device = ""
    @api_token = ""
    @api_password = ""
    refresh_header
  end
  
  def refresh_header
    @authenticate = {device: @api_device, email: @api_email, token: @api_token, password: @api_password}
    @request.headers["Accept"] = "application/json"
  end
  
  def make_data(data)
    {data: data}.merge @authenticate
  end
  
  def make_values(values)
    values.merge @authenticate
  end
  
  def can_test?(route)
    !can_not_test? route
  end
  
  def can_not_test?(route)
    if !@test_default.include?(route)
      return true
    end
    if respond_to? "init_" + route.to_s
      send "init_" + route.to_s
    end
    false
  end
  
end