require 'application_controller_test'

module Frontend
  class AccessControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::AccessController.new
      @model_class = Access
      @objects = [accesses(:access1), accesses(:access2)]
      @related = products(:product1)
      @test_default = [:create]
    end
    
  end
end