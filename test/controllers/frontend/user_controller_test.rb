require 'application_controller_test'

module Frontend
  class UserControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::UserController.new
      @model_class = User
      @objects = [users(:user1), users(:user2)]
      @attribute_string = [:name, :email, :facebook]
      @attribute_boolean = [:can_notification]
      @is_unique_company = [:email]
      @test_default = [:create]
    end
    
  end
end