require 'application_controller_test'

module Frontend
  class VoteControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::VoteController.new
      @company = companies(:company1)
      @user = users(:user1)
      @relateds = [
        {route: "product", type: "Product", id: products(:product2).id},
        {route: "reward", type: "Reward", id: rewards(:reward2).id},
      ]
    end
    
    def test_create_and_update
      for related in @relateds
        related_route = related[:route]
        related_type = related[:type]
        related_id = related[:id]
        data = {related_id: related_id}
        vote = 1
        data[:vote] = vote
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_custom_data data, result, vote.to_f.to_s, vote, related_type, related_id
        vote = 10
        data[:vote] = vote
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_custom_data data, result, vote.to_f.to_s, vote, related_type, related_id
        vote = 0
        data[:vote] = vote
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_custom_data data, result, vote.to_f.to_s, vote, related_type, related_id
        vote = -1
        data[:vote] = vote
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_custom_data data, result, 0.to_f.to_s, 0, related_type, related_id
        vote = 11
        data[:vote] = vote
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_custom_data data, result, 10.to_f.to_s, 10, related_type, related_id
        vote = ""
        data[:vote] = vote
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_error_invalid data, result
        data[:related_id] = ""
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_error_badrequest data, result
      end
    end
    
    def assert_find_custom_data(data, result, vote_result, vote_entity, related_type, related_id)
      assert_success data, result
      assert_equal vote_result, result["success"]["vote"], "REQUEST: " + data.to_s + "\n" + "RESPONSE: " + result.to_s
      entity = Vote.where(related_id: related_id, related_type: related_type, user: @user, company: @company).first
      assert entity
      assert_equal entity.vote, vote_entity
    end
    
  end
end