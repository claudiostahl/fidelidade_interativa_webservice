require 'application_controller_test'

module Frontend
  class SharedControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::SharedController.new
      @model_class = Shared
      @objects = [shareds(:shared1), shareds(:shared2)]
      @related = products(:product1)
      @test_default = [:create]
    end
    
  end
end