require 'application_controller_test'

module Frontend
  class CouponReadControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::CouponReadController.new
      @model_class = CouponRead
      @objects = [coupon_reads(:coupon_read1), coupon_reads(:coupon_read2)]
      @attribute_default = {coupon: coupons(:coupon1).token}
      @test_default = [:create]
    end
    
  end
end