require 'application_controller_test'

module Frontend
  class CashflowControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::CashflowController.new
      @model_class = Cashflow
      @objects = [cashflows(:cashflow1), cashflows(:cashflow2)]
      @test_default = [:index]
    end
    
  end
end