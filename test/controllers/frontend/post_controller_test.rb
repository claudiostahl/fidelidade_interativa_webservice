require 'application_controller_test'

module Frontend
  class PostControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::PostController.new
      @model_class = Post
      @objects = [posts(:post1), posts(:post2)]
      @attribute_string = [:title, :slug, :description, :content]
      @attribute_boolean = [:enabled]
      @attribute_datetime = [:publication_date]
      @is_unique_company = [:slug]
      @test_default = [:index]
    end
    
  end
end