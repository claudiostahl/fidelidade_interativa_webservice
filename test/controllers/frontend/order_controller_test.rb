require 'application_controller_test'

module Frontend
  class OrderControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::OrderController.new
      @model_class = Order
      @objects = [orders(:order1), orders(:order2)]
      @attribute_default = {reward: rewards(:reward1).id}
      @test_default = [:create, :index]
    end
    
  end
end