require 'application_controller_test'

module Frontend
  class NotificationControllerTest < ApplicationControllerTest
    
    def init
      config_frontend
      @controller = Frontend::NotificationController.new
      @model_class = Notification
      @objects = [notifications(:notification1), notifications(:notification2)]
      @attribute_boolean = [:read, :arquived]
      @test_default = [:update, :index, :view]
    end
    
  end
end