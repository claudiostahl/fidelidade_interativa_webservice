require 'application_controller_test'

module Backend
  class OrderCanceledControllerTest < ApplicationControllerTest
    
    def config
      config_backend
      @controller = Backend::OrderCanceledController.new
      @model_class = OrderCanceled
      @objects = [order_canceleds(:order_canceled1), order_canceleds(:order_canceled2)]
      @attribute_default = {order: orders(:order3).id}
      @attribute_string = [:comments]
      @is_required = [:order]
      @not_required = [:comments]
      @max_lenght = {comments: 65536}
      @has_one = {order: orders(:order3).id}
      @can_many = false
      @test_default = [:create, :update, :index, :view]
    end
    
    def init_update
      @is_required = []
      @has_one = {}
      apply_config
    end
    
  end
end