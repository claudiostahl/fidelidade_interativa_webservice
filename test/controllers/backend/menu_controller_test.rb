require 'application_controller_test'

module Backend
  class MenuControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::MenuController.new
      @model_class = Menu
      @objects = [menus(:menu1), menus(:menu2)]
      @attribute_string = [:title, :url, :content]
      @attribute_image = [:image]
      @attribute_integer = [:order]
      @attribute_boolean = [:enabled]
      @is_required = [:title, :enabled]
      @not_required = [:order, :image, :url, :content]
      @max_lenght = {title: 200, url: 200, content: 65536}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end