require 'application_controller_test'

module Backend
  class RewardPhotoControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::RewardPhotoController.new
      @model_class = Photo
      @objects = [photos(:photo3), photos(:photo4)]
      @attribute_string = [:code]
      @attribute_boolean = [:enabled]
      @attribute_integer = [:order]
      @attribute_image = [:image]
      @related = rewards(:reward1)
      @is_required = [:code]
      @max_lenght = {code: 80}
      @has_one = {related: rewards(:reward1).id}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end