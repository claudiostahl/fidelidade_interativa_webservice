require 'application_controller_test'

module Backend
  class PremiumTypeControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::PremiumTypeController.new
      @model_class = PremiumType
      @objects = [premium_types(:premium_type1), premium_types(:premium_type2)]
      @attribute_string = [:name, :description, :notification_title, :notification_description]
      @attribute_number = [:value]
      @attribute_integer = [:limit, :limit_user]
      @attribute_datetime = [:validity]
      @attribute_boolean = [:enabled]
      @is_required = [:name, :description, :notification_title, :notification_description, :value, :limit, :limit_user, :validity, :enabled]
      @max_lenght = {name: 200, description: 65536, notification_title: 200, notification_description: 65536}
      @unallowed = {:token => "a"}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end