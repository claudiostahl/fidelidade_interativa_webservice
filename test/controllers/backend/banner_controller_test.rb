require 'application_controller_test'

module Backend
  class BannerControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::BannerController.new
      @model_class = Banner
      @objects = [banners(:banner1), banners(:banner2)]
      @attribute_string = [:name, :link]
      @attribute_image = [:image]
      @attribute_boolean = [:enabled]
      @attribute_integer = [:order]
      @is_required = [:name, :enabled, :image]
      @not_required = [:order, :link]
      @max_lenght = {name: 200, link: 200}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end