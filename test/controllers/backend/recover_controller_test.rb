require 'application_controller_test'

module Backend
  class RecoverControllerTest < ApplicationControllerTest
      
    def init
      config_backend
      @controller = Backend::RecoverController.new
      @email = "administrator1@company.com"
    end
  
    test "create ok" do
      data = {login: @email}
      post :create, data
      result = JSON.parse(@response.body)
      assert_success data, result
    end
    
    test "create invalid email" do
      data = {login: "kk"}
      post :create, data
      result = JSON.parse(@response.body)
      assert_error_badrequest data, result
    end
    
    test "create missing email" do
      data = {}
      post :create, data
      result = JSON.parse(@response.body)
      assert_error_badrequest data, result
    end
    
    test "update ok" do
      data = {token: "asdfghjkl", password: "123456"}
      put :update, data
      result = JSON.parse(@response.body)
      assert_success data, result
    end
    
    test "update invalid token" do
      data = {token: "ooo", password: "123456"}
      put :update, data
      result = JSON.parse(@response.body)
      assert_error_badrequest data, result
    end
    
    test "update missing" do
      data = {}
      put :update, data
      result = JSON.parse(@response.body)
      assert_error_badrequest data, result
    end
    
    test "update missing password" do
      data = {token: "asdfghjkl"}
      put :update, data
      result = JSON.parse(@response.body)
      assert_error_badrequest data, result
    end
    
  end
end