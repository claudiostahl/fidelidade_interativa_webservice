require 'application_controller_test'

module Backend
  class ProductPhotoControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::ProductPhotoController.new
      @model_class = Photo
      @objects = [photos(:photo1), photos(:photo2)]
      @attribute_string = [:code]
      @attribute_boolean = [:enabled]
      @attribute_integer = [:order]
      @attribute_image = [:image]
      @related = products(:product1)
      @is_required = [:code]
      @max_lenght = {code: 80}
      @has_one = {related: products(:product1).id}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end