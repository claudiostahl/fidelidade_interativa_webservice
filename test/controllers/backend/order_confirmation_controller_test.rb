require 'application_controller_test'

module Backend
  class OrderConfirmationControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::OrderConfirmationController.new
      @model_class = OrderConfirmation
      @objects = [order_confirmations(:order_confirmation1), order_confirmations(:order_confirmation2)]
      @attribute_default = {order: orders(:order1).id}
      @attribute_string = [:comments]
      @is_required = [:order]
      @not_required = [:comments]
      @max_lenght = {comments: 65536}
      @has_one = {order: orders(:order1).id}
      @test_default = [:create, :update, :index, :view]
    end
    
    def init_update
      @is_required = []
      @has_one = {}
      apply_config
    end
    
  end
end