require 'application_controller_test'

module Backend
  class PostControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::PostController.new
      @model_class = Post
      @objects = [posts(:post1), posts(:post2)]
      @attribute_string = [:title, :slug, :description, :content]
      @attribute_boolean = [:enabled]
      @attribute_datetime = [:publication_date]
      @is_required = [:title, :slug, :description, :content]
      @is_unique_company = [:slug]
      @max_lenght = {title: 200, slug: 200, description: 65536, content: 65536}
      @can_many = false
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end