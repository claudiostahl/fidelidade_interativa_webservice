require 'application_controller_test'

module Backend
  class LanguageControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::LanguageController.new
      @model_class = Language
      @objects = [languages(:language_pt_br), languages(:language_en)]
      @attribute_small_string = [:name, :abbreviation]
      @is_required = [:name, :abbreviation]
      @is_unique = [:name, :abbreviation]
      @max_lenght = {name: 80, abbreviation: 10}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end