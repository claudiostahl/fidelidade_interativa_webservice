require 'application_controller_test'

module Backend
  class AdministratorControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::AdministratorController.new
      @model_class = Administrator
      @objects = [administrators(:administrator1), administrators(:administrator2)]
      @attribute_string = [:name]
      @attribute_email = [:email]
      @attribute_boolean = [:enabled, :is_root]
      @attribute_integer = [:timezone]
      @attribute_password = [:password]
      @is_required = [:name, :email, :enabled, :is_root, :password]
      @not_required = [:timezone, :language, :companies, :resources]
      @is_unique = [:email]
      @max_lenght = {name: 200, email: 200}
      @unallowed = {:recover_token => "a"}
      @has_many = {companies: [companies(:company1).id, companies(:company2).id], resources: ["administrator", "user"]}
      @has_one = {language: languages(:language_pt_br).id}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
    test "create other company with default credentials" do
      @request.headers["Api-Device"] = "device_default"
      @request.headers["Api-Token"] = "token_default"
      data = inflate_data
      data["companies"] = [companies(:company2).id]
      post :create, data: data
      result = JSON.parse(@response.body)
      assert result.has_key?("error"), "REQUEST: " + data.to_json + "\n" + "RESPONSE: " + result.to_json
      assert_equal result["error"]["code"], 401, "REQUEST: " + data.to_json + "\n" + "RESPONSE: " + result.to_json
    end
    
  end
end