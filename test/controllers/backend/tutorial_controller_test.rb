require 'application_controller_test'

module Backend
  class TutorialControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::TutorialController.new
      @model_class = Tutorial
      @objects = [tutorials(:tutorial1), tutorials(:tutorial2)]
      @attribute_string = [:name]
      @attribute_image = [:image]
      @attribute_integer = [:order]
      @attribute_boolean = [:enabled]
      @is_required = [:name, :enabled, :image]
      @not_required = [:order]
      @max_lenght = {name: 200}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end