require 'application_controller_test'

module Backend
  class CouponGroupControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::CouponGroupController.new
      @model_class = CouponGroup
      @objects = [coupon_groups(:coupon_group1), coupon_groups(:coupon_group2)]
      @attribute_string = [:name]
      @attribute_integer = [:order, :limit]
      @is_required = [:name]
      @attribute_number = [:value]
      @attribute_datetime = [:validity]
      @attribute_boolean = [:enabled]
      @is_required = [:value, :validity, :limit, :enabled]
      @not_required = [:order]
      @max_lenght = {name: 200}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end