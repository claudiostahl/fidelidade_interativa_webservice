require 'application_controller_test'

module Backend
  class RewardControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::RewardController.new
      @model_class = Reward
      @objects = [rewards(:reward1), rewards(:reward2)]
      @attribute_string = [:name, :slug, :short_description, :description, :rule_description]
      @attribute_integer = [:limit, :limit_user]
      @attribute_number = [:value, :price, :first_vote]
      @attribute_boolean = [:enabled]
      @attribute_date = [:date_start, :date_finish]
      @attribute_datetime = [:publication_date]
      @is_required = [:name, :slug, :short_description, :description, :rule_description, :value, :first_vote, :publication_date, :limit, :limit_user, :date_start, :date_finish, :enabled]
      @is_unique_company = [:slug]
      @max_lenght = {name: 200, slug: 200, short_description: 200, description: 65536, rule_description: 65536}
      @unallowed = {:rate => "10"}
      @has_one = {user_group: user_groups(:user_group1).id}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end