require 'application_controller_test'

module Backend
  class PremiumControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::PremiumController.new
      @model_class = Premium
      @objects = [premiums(:premium1), premiums(:premium2)]
      @attribute_default = {:user => users(:user1).id, :premium_type => premium_types(:premium_type1).id}
      @attribute_string = [:campaign]
      @attribute_number = [:value]
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end