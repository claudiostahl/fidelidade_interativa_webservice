require 'application_controller_test'

module Backend
  class ProductControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::ProductController.new
      @model_class = Product
      @objects = [products(:product1), products(:product2)]
      @attribute_string = [:name, :slug, :short_description, :description]
      @attribute_number = [:price, :special_price, :first_vote]
      @attribute_boolean = [:enabled]
      @is_required = [:name, :slug, :price, :special_price, :first_vote]
      @is_unique_company = [:slug]
      @max_lenght = {name: 200, slug: 200, short_description: 200, description: 65536}
      @unallowed = {:rate => "10"}
      @has_many = {related_products: [products(:product1).id, products(:product2).id]}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end