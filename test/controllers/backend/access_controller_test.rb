require 'application_controller_test'

module Backend
  class AccessControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::AccessController.new
      @model_class = Access
      @objects = [accesses(:access1), accesses(:access2)]
      @test_default = [:index]
    end
    
  end
end