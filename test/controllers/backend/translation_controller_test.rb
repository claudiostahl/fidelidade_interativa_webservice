require 'application_controller_test'

module Backend
  class TranslationControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::TranslationController.new
      @company = companies(:company1)
      @language = languages(:language_pt_br)
      @relateds = [
        {route: "banner", type: "Banner", id: banners(:banner1).id},
        # {route: "product_block", type: "Block", id: blocks(:block1).id},
        # {route: "reward_block", type: "Block", id: blocks(:block1).id},
        # {route: "post", type: "Post", id: posts(:post1).id},
        # {route: "product", type: "Product", id: products(:product1).id},
        # {route: "reward", type: "Reward", id: rewards(:reward1).id},
        # {route: "tutorial", type: "Tutorial", id: tutorials(:tutorial1).id},
      ]
    end
    
    def test_create_and_update
      for related in @relateds
        related_route = related[:route]
        related_type = related[:type]
        related_id = related[:id]
        # string
        data = {code: "translation_test", language: @language.abbreviation, related_id: related_id}
        value = "value_string"
        data[:value] = value
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, value, value, :value_string, related_type, related_id
        # number integer
        data[:value] = 10
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, "10.0", 10.0, :value_number, related_type, related_id
        # number float
        data[:value] = 10.55
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, "10.55", 10.55, :value_number, related_type, related_id
        # datetime
        data[:value] = "2015-01-01T10:10:10+03:00"
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, "2015-01-01T07:10:10+00:00", "2015-01-01T07:10:10+00:00".to_datetime, :value_datetime, related_type, related_id
        #boolean true
        value = true
        data[:value] = value
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, value, value, :value_boolean, related_type, related_id
        #boolean false
        value = false
        data[:value] = value
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, value, value, :value_boolean, related_type, related_id
        # text
        value = "A" * 65536
        data[:value] = value
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, value, value, :value_text, related_type, related_id
        # empty
        value = nil
        data[:value] = value
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, value, value, :value_string, related_type, related_id
        # blank
        value = ""
        data[:value] = value
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, value, value, :value_string, related_type, related_id
        # image
        value = nil
        data[:value] = value
        data[:image] = fixture_file_upload("images/photo.jpg", 'image/jpeg', :binary)
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_find_config_data data, result, value, value, :value_string, related_type, related_id
        assert_image result, :image
        # code empty
        data[:code] = ""
        put :update, data: data, related: related_route
        result = JSON.parse(@response.body)
        assert_error_invalid data, result
      end
    end
    
    def assert_find_config_data(data, result, value_result, value_entity, attribute, related_type, related_id, has_image = false)
      assert_success data, result
      code = data[:code]
      assert_equal value_result, result["success"]["value"], "REQUEST: " + data.to_s + "\n" + "RESPONSE: " + result.to_s
      entity = Translation.where(related_id: related_id, related_type: related_type, code: code, company: @company, language: @language.id).first
      assert entity, "attribute: " + attribute.to_s + "\n" + "value: " + value_entity.to_s
      assert_equal entity.attributes[attribute.to_s], value_entity, "attribute: " + attribute.to_s + "\n" + "value: " + value_entity.to_s
      if attribute != :value_number
        assert_nil entity.attributes["value_number"]
      end
      if attribute != :value_datetime
        assert_nil entity.attributes["value_datetime"]
      end
      if attribute != :value_boolean
        assert_nil entity.attributes["value_boolean"]
      end
      if attribute != :value_text
        assert_nil entity.attributes["value_text"]
      end
      if attribute != :value_string
        assert_nil entity.attributes["value_string"]
      end
      if has_image
        assert_equal entity.attributes[attribute.to_s + "_file_name"], "photo.jpg"
        assert_equal entity.attributes[attribute.to_s + "_content_type"], "image/jpeg"
        assert_equal entity.attributes[attribute.to_s + "_file_size"], 645937
      end
    end
    
  end
end