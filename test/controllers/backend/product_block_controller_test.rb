require 'application_controller_test'

module Backend
  class ProductBlockControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::ProductBlockController.new
      @model_class = Block
      @objects = [blocks(:block1), blocks(:block2)]
      @attribute_string = [:name]
      @attribute_integer = [:layout, :order]
      @attribute_image = [:image]
      @is_required = [:name, :layout, :order]
      @max_lenght = {name: 200}
      @unallowed = {:group => 3}
      @has_one = {parent: blocks(:block1).id, related: products(:product1).id}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end