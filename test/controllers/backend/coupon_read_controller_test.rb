require 'application_controller_test'

module Backend
  class CouponReadControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::CouponReadController.new
      @model_class = CouponRead
      @objects = [coupon_reads(:coupon_read1), coupon_reads(:coupon_read2)]
      @attribute_default = {:coupon => coupons(:coupon1).id, :user => users(:user1).id}
      @attribute_number = [:value]
      @test_default = [:index]
    end
    
  end
end