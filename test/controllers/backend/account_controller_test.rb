require 'application_controller_test'

module Backend
  class AccountControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::AccountController.new
      @model_class = Administrator
      @entity = administrators(:administrator_root)
    end
    
    def test_update_ok
      name = "teste"
      email = "teste@email.com"
      password = "12345678"
      data = {name: name, email: email, password: password}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_success data, result
      model = Administrator.find(@entity.id)
      assert_equal model.name, name
      assert_equal model.email, email
      assert Administrator.authenticate email, password
    end
    
    def test_update_invalid_email
      email = "email"
      data = {email: email}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_error_invalid data, result
      model = Administrator.find(@entity.id)
      assert_equal model.name, @entity.name
      assert_not_equal model.email, email
    end
    
    def test_update_empty_name
      data = {name: ""}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_error_invalid data, result
      model = Administrator.find(@entity.id)
      assert_equal model.name, @entity.name
      assert_not_equal model.name, ""
    end
    
    def test_update_empty_email
      data = {email: ""}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_error_invalid data, result
      model = Administrator.find(@entity.id)
      assert_equal model.email, @entity.email
      assert_not_equal model.email, ""
    end
    
    def test_update_empty_password
      data = {password: ""}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_error_invalid data, result
      model = Administrator.find(@entity.id)
      assert_not Administrator.authenticate model.email, ""
    end
    
  end
end