require 'application_controller_test'

module Backend
  class SharedControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::SharedController.new
      @model_class = Shared
      @objects = [shareds(:shared1), shareds(:shared2)]
      @test_default = [:index]
    end
    
  end
end