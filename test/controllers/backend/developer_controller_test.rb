require 'application_controller_test'

module Backend
  class DeveloperControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::DeveloperController.new
      @model_class = Developer
      @objects = [developers(:developer1), developers(:developer2)]
      @attribute_string = [:name]
      @attribute_email = [:email]
      @attribute_boolean = [:enabled]
      @is_required = [:email]
      @is_unique_company = [:email]
      @max_lenght = {email: 200}
      @has_many = {resources: ["administrator", "user"]}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end