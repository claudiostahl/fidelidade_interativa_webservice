require 'application_controller_test'

module Backend
  class RewardRuleControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::RewardRuleController.new
      @model_class = RewardRule
      @objects = [reward_rules(:reward_rule1), reward_rules(:reward_rule2)]
      @attribute_default = {:reward => rewards(:reward1).id}
      @attribute_integer = [:week_day]
      @attribute_time = [:hour_start, :hour_finish]
      @has_one = {:reward => rewards(:reward1).id}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end