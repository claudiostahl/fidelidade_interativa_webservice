require 'application_controller_test'

module Backend
  class ConfigControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::ConfigController.new
      @model_class = Config
      @objects = [configs(:config1), configs(:config2)]
    end
    
    def test_create_and_update
      # string
      value = "value_string"
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, value, value, :value_string
      # number integer
      value = 10
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, "10.0", 10.0, :value_number
      # number float
      value = 10.55
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, "10.55", 10.55, :value_number
      # datetime
      value = "2015-01-01T10:10:10+03:00"
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, "2015-01-01T07:10:10+00:00", "2015-01-01T07:10:10+00:00".to_datetime, :value_datetime
      #boolean true
      value = true
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, value, value, :value_boolean
      #boolean false
      value = false
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, value, value, :value_boolean
      # text
      value = "A" * 65536
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, value, value, :value_text
      # empty
      value = nil
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, value, value, :value_string
      # blank
      value = ""
      data = {code: "config_test", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_find_config_data data, result, value, value, :value_string
    end
    
    def test_empty
      value = "abc"
      data = {code: "", value: value}
      put :update, data: data
      result = JSON.parse(@response.body)
      assert_error_invalid data, result
    end
    
    def assert_find_config_data(data, result, value_result, value_entity, attribute)
      assert_success data, result
      assert_equal value_result, result["success"]["value"], "REQUEST: " + data.to_s + "\n" + "RESPONSE: " + result.to_s
      entity = Config.where(code: data[:code]).first
      assert entity, "attribute: " + attribute.to_s + "\n" + "value: " + value_entity.to_s
      assert_equal entity.attributes[attribute.to_s], value_entity, "attribute: " + attribute.to_s + "\n" + "value: " + value_entity.to_s
      if attribute != :value_number
        assert_nil entity.attributes["value_number"]
      end
      if attribute != :value_datetime
        assert_nil entity.attributes["value_datetime"]
      end
      if attribute != :value_boolean
        assert_nil entity.attributes["value_boolean"]
      end
      if attribute != :value_text
        assert_nil entity.attributes["value_text"]
      end
      if attribute != :value_string
        assert_nil entity.attributes["value_string"]
      end
    end
    
  end
end