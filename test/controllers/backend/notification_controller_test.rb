require 'application_controller_test'

module Backend
  class NotificationControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::NotificationController.new
      @model_class = Notification
      @objects = [notifications(:notification1), notifications(:notification2)]
      @attribute_default = {:user => users(:user1).id}
      @attribute_string = [:name, :description]
      @attribute_boolean = [:read, :arquived]
      @test_default = [:index]
    end
    
  end
end