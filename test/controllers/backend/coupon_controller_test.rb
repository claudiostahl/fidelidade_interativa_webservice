require 'application_controller_test'

module Backend
  class CouponControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::CouponController.new
      @model_class = Coupon
      @objects = [coupons(:coupon1), coupons(:coupon2)]
      @is_required = [:coupon_group]
      @has_one = {coupon_group: coupon_groups(:coupon_group1).id}
      @unallowed = {:token => "a", :used => 6}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end
