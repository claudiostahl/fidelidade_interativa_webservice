require 'application_controller_test'

module Backend
  class UserControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::UserController.new
      @model_class = User
      @objects = [users(:user1), users(:user2)]
      @attribute_string = [:name, :facebook]
      @attribute_email = [:email]
      @attribute_boolean = [:can_notification, :enabled]
      @attribute_integer = [:timezone]
      @attribute_password = [:password]
      @is_required = [:name, :email, :can_notification, :enabled]
      @not_required = [:language, :facebook, :timezone]
      @is_unique_company = [:email]
      @max_lenght = {name: 200, email: 200, facebook: 200}
      @unallowed = {:token => "a", :recover_token => "b"}
      @test_default = [:update, :index, :view]
    end
    
  end
end