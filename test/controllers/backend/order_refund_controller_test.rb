require 'application_controller_test'

module Backend
  class OrderRefundControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::OrderRefundController.new
      @model_class = OrderRefund
      @objects = [order_refunds(:order_refund1), order_refunds(:order_refund2)]
      @attribute_default = {order: orders(:order3).id}
      @attribute_string = [:comments]
      @attribute_number = [:value]
      @is_required = [:order, :value]
      @not_required = [:comments]
      @max_lenght = {comments: 65536}
      @has_one = {order: orders(:order3).id}
      @can_many = false
      @test_default = [:create, :update, :index, :view]
    end
    
    def init_update
      @attribute_number = []
      @is_required = []
      @has_one = {}
      apply_config
    end
    
  end
end