require 'application_controller_test'

module Backend
  class OrderControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::OrderController.new
      @model_class = Order
      @objects = [orders(:order1), orders(:order2)]
      @attribute_default = {user: users(:user1).id, reward: rewards(:reward1).id}
      @attribute_string = [:comments]
      @is_required = [:user, :reward]
      @not_required = [:comments]
      @max_lenght = {comments: 65536}
      @has_one = {user: users(:user1).id, reward: rewards(:reward1).id}
      @test_default = [:create, :update, :index, :view]
    end
    
    def init_update
      @is_required = []
      @has_one = {}
      apply_config
    end
    
  end
end