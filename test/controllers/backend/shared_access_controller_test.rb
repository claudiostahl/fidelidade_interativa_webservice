require 'application_controller_test'

module Backend
  class SharedAccessControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::SharedAccessController.new
      @model_class = SharedAccess
      @objects = [shared_accesses(:shared_access1), shared_accesses(:shared_access2)]
      @test_default = [:index]
    end
    
  end
end