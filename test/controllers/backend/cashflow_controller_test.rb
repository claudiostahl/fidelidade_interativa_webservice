require 'application_controller_test'

module Backend
  class CashflowControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::CashflowController.new
      @model_class = Cashflow
      @objects = [cashflows(:cashflow1), cashflows(:cashflow2)]
      @test_default = [:index]
    end
    
  end
end