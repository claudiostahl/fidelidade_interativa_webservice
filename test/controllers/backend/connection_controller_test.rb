require 'application_controller_test'

module Backend
  class ConnectionControllerTest < ApplicationControllerTest
      
    def init
      config_backend
      @controller = Backend::ConnectionController.new
      @company = "company"
      @password = "102030"
    end
  
    test "login root" do
      data = {company: @company, login: "root@company.com", password: @password}
      post :login, data
      result = JSON.parse(@response.body)
      assert_success data, result
    end
    
    test "login default" do
      data = {company: @company, login: "admin@company.com", password: @password}
      post :login, data
      result = JSON.parse(@response.body)
      assert_success data, result
    end
    
    test "login default disabled" do
      data = {company: @company, login: "admin_disabled@company.com", password: @password}
      post :login, data
      result = JSON.parse(@response.body)
      assert_error_unauthorized data, result
    end
    
    test "login invalid" do
      data = {company: @company, login: "root@company.com", password: "123"}
      post :login, data
      result = JSON.parse(@response.body)
      assert_error_badrequest data, result
    end
    
    test "empty credentials" do
      data = {company: @company, password: "123"}
      post :login, data
      result = JSON.parse(@response.body)
      assert_error_badrequest data, result
    end
    
    test "logout" do
      post :logout
      result = JSON.parse(@response.body)
      assert_success nil, result
      connection = Connection.where(device: @api_device).first
      assert_nil connection
    end
    
  end
end