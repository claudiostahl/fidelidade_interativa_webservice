require 'application_controller_test'

module Backend
  class MessageControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::MessageController.new
      @model_class = Message
      @objects = [messages(:message1), messages(:message2)]
      @attribute_default = {:users => [users(:user1).id, users(:user2).id]}
      @attribute_string = [:link, :name, :description]
      @attribute_image = [:image]
      @attribute_datetime = [:publication_date]
      @is_required = [:link, :name, :description]
      @max_lenght = {link: 200, name: 200, description: 65536}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end