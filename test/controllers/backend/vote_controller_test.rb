require 'application_controller_test'

module Backend
  class VoteControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::VoteController.new
      @model_class = Vote
      @objects = [votes(:vote1), votes(:vote2)]
      @attribute_default = {:user => users(:user1).id}
      @attribute_number = [:vote]
      @related = products(:product1)
      @test_default = [:index]
    end
    
  end
end