require 'application_controller_test'

module Backend
  class CompanyControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::CompanyController.new
      @model_class = Company
      @objects = [companies(:company2), companies(:company3)]
      @attribute_string = [:name, :code]
      @attribute_boolean = [:enabled_backend, :enabled_frontend, :enabled_api]
      @is_unique = [:code]
      @is_required = [:code, :name, :enabled_backend, :enabled_frontend, :enabled_api]
      @max_lenght = {name: 200, code: 200}
      @unallowed = {:token => "a"}
      @has_many = {packages: [packages(:package_product).id, packages(:package_post).id]}
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end