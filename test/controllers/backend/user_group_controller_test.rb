require 'application_controller_test'

module Backend
  class UserGroupControllerTest < ApplicationControllerTest
    
    def init
      config_backend
      @controller = Backend::UserGroupController.new
      @model_class = UserGroup
      @objects = [user_groups(:user_group1), user_groups(:user_group2)]
      @attribute_string = [:name]
      @max_lenght = {name: 200}
      @is_required = [:name]
      @test_default = [:create, :update, :destroy, :index, :view]
    end
    
  end
end