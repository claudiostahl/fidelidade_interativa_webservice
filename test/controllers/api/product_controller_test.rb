require 'application_controller_test'

module Api
  class ProductControllerTest < ApplicationControllerTest
    
    def init
      config_api
      @controller = Api::ProductController.new
      @model_class = Product
      @objects = [products(:product1), products(:product2)]
      @test_default = [:index, :view]
    end
    
  end
end