require 'application_controller_test'

module Api
  class PostControllerTest < ApplicationControllerTest
    
    def init
      config_api
      @controller = Api::PostController.new
      @model_class = Post
      @objects = [posts(:post1), posts(:post2)]
      @test_default = [:index, :vi]
    end
    
  end
end