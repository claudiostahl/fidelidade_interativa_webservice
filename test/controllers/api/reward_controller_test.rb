require 'application_controller_test'

module Api
  class RewardControllerTest < ApplicationControllerTest
    
    def init
      config_api
      @controller = Api::RewardController.new
      @model_class = Reward
      @objects = [rewards(:reward1), rewards(:reward2)]
      @test_default = [:index, :view]
    end
    
  end
end