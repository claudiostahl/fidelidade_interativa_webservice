require 'application_controller_test'

module Api
  class UserControllerTest < ApplicationControllerTest
    
    def init
      config_api
      @controller = Api::UserController.new
      @model_class = User
      @objects = [users(:user1), users(:user2)]
      @test_default = [:index, :view]
    end
    
  end
end