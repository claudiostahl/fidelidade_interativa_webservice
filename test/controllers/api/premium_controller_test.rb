require 'application_controller_test'

module Api
  class PremiumControllerTest < ApplicationControllerTest
    
    def init
      config_api
      @controller = Api::PremiumController.new
      @model_class = Premium
      @objects = [premiums(:premium1), premiums(:premium2)]
      @attribute_default = {:user => users(:user1).token, :premium_type => premium_types(:premium_type1).token}
      @attribute_string = [:campaign]
      @attribute_number = [:value]
      @has_one = {:user => users(:user1).token, :premium_type => premium_types(:premium_type1).token}
      @test_default = [:create, :index, :view]
    end
    
  end
end