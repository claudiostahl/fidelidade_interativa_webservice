require 'application_controller_test'

module ApplicationControllerTestDefault
  
  def test_crete
    return if can_not_test? :create
    data = inflate_data
    post :create, make_data(data)
    result = JSON.parse(@response.body)
    assert_success data, result
    assert_find_many_data data
  end
  
  # def test_crete_blank
  #   return if can_not_test? :create
  #   data = ""
  #   post :create, data: data
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest data, result
  # end
  
  # def test_crete_empty
  #   return if can_not_test? :create
  #   post :create
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  # end
  
  # def test_crete_invalid
  #   return if can_not_test? :create
  #   data = "aaa"
  #   post :create, data: data
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest data, result
  # end
  
  # def test_crete_many
  #   return if can_not_test? :create || !@can_many
  #   data = []
  #   data << inflate_data(1)
  #   data << inflate_data(2)
  #   data << inflate_data(3)
  #   post :create, data: data
  #   result = JSON.parse(@response.body)
  #   assert_success data[0], result[0]
  #   assert_success data[1], result[1]
  #   assert_success data[2], result[2]
  # end
  
  # def test_crete_many_invalid
  #   return if can_not_test? :create || !@can_many
  #   data = []
  #   data << inflate_data(1)
  #   data << ""
  #   data << "lkjh"
  #   post :create, data: data
  #   result = JSON.parse(@response.body)
  #   assert_success data[0], result[0]
  #   assert_error_badrequest data[1], result[1]
  #   assert_error_badrequest data[2], result[2]
  # end
  
  # def test_create_without_authentication
  #   return if can_not_test? :create
  #   clear_header
  #   data = inflate_data
  #   post :create, data: data
  #   result = JSON.parse(@response.body)
  #   assert_error_unauthorized data, result
  #   assert_find_many_invalid_data data
  # end
  
  # def test_update
  #   return if can_not_test? :update
  #   for object in @objects
  #     data = inflate_data object.id, object.attributes
  #     data.delete "id"
  #     put :update, data: data, id: object.id
  #     result = JSON.parse(@response.body)
  #     assert_success data, result
  #     assert_find_many_data data
  #   end
  # end
  
  # def test_update_blank
  #   return if can_not_test? :update
  #   for object in @objects
  #     data = object.attributes
  #     data = inflate_data object.id, data
  #     data.delete "id"
  #     put :update, data: "", id: object.id
  #     result = JSON.parse(@response.body)
  #     assert_error_badrequest data, result
  #   end
  # end
  
  # def test_update_empty
  #   return if can_not_test? :update
  #   for object in @objects
  #     data = object.attributes
  #     data = inflate_data object.id, data
  #     data.delete "id"
  #     put :update, id: object.id
  #     result = JSON.parse(@response.body)
  #     assert_error_badrequest data, result
  #   end
  # end
  
  # def test_update_invalid
  #   return if can_not_test? :update
  #   for object in @objects
  #     data = object.attributes
  #     data = inflate_data object.id, data
  #     data.delete "id"
  #     put :update, data: "abc", id: object.id
  #     result = JSON.parse(@response.body)
  #     assert_error_badrequest data, result
  #   end
  # end
  
  # def test_update_many
  #   return if can_not_test? :update || !@can_many
  #   data = []
  #   for object in @objects
  #     d = object.attributes
  #     d = inflate_data object.id, d
  #     data << d
  #   end
  #   put :update, data: data
  #   result = JSON.parse(@response.body)
  #   for i in (0..@objects.count-1) do
  #     assert_success data[i], result[i]
  #     assert_find_many_data data[i]
  #   end
  # end
  
  # def test_update_many_invalid
  #   return if can_not_test? :update || !@can_many
  #   d = inflate_data @objects[0].id, @objects[0].attributes
  #   data = [d, "", "abc", {id: "aa"}]
  #   put :update, data: data
  #   result = JSON.parse(@response.body)
  #   assert_success data[0], result[0]
  #   assert_error_badrequest data[1], result[1]
  #   assert_error_badrequest data[2], result[2]
  #   assert_error_notfound data[3], result[3]
  # end
  
  # def test_update_invalid_id
  #   return if can_not_test? :update
  #   data = inflate_data
  #   put :update, data: data, id: "invalid"
  #   result = JSON.parse(@response.body)
  #   assert_error_notfound data, result
  #   assert_find_many_invalid_data data
  # end
  
  # def test_update_without_authentication
  #   return if can_not_test? :update
  #   clear_header
  #   data = inflate_data @objects[0].id, @objects[0].attributes
  #   put :update, data: data
  #   result = JSON.parse(@response.body)
  #   assert_error_unauthorized data, result
  #   assert_find_many_invalid_data data
  # end
  
  # def test_destroy
  #   return if can_not_test? :destroy
  #   for object in @objects
  #     delete :destroy, id: object.id
  #     result = JSON.parse(@response.body)
  #     assert_success nil, result
  #     entity = @model_class.where(id: object.id).first
  #     assert_nil entity, "REQUEST: " + object.to_s + "\n" + "RESPONSE: " + result.to_s
  #   end
  # end
  
  # def test_destroy_many
  #   return if can_not_test? :destroy || !@can_many
  #   data = []
  #   for object in @objects
  #     data << object.id
  #   end
  #   delete :destroy, data: data
  #   result = JSON.parse(@response.body)
  #   for i in (0..@objects.count-1) do
  #     assert_success data[i], result[i]
  #   end
  #   for object in @objects
  #     entity = @model_class.where(id: object.id).first
  #     assert_nil entity, "REQUEST: " + data.to_s + "\n" + "RESPONSE: " + result.to_s
  #   end
  # end
  
  # def test_destroy_empty
  #   return if can_not_test? :destroy
  #   delete :destroy
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  # end
  
  # def test_destroy_notfound
  #   return if can_not_test? :destroy
  #   delete :destroy, id: "invalid"
  #   result = JSON.parse(@response.body)
  #   assert_error_notfound nil, result
  # end
  
  # def test_destroy_many_notfound
  #   return if can_not_test? :destroy || !@can_many
  #   data = ["a", "b"]
  #   delete :destroy, data: data
  #   result = JSON.parse(@response.body)
  #   assert_error_notfound data[0], result[0]
  #   assert_error_notfound data[1], result[1]
  # end
  
  # def test_destroy_many_badrequest
  #   return if can_not_test? :destroy || !@can_many
  #   data = [{a:"b"}, {b: 44}]
  #   delete :destroy, data: data
  #   result = JSON.parse(@response.body)
  #   assert_error_notfound data[0], result[0]
  #   assert_error_notfound data[1], result[1]
  # end
  
  # def test_destroy_without_authentication
  #   return if can_not_test? :destroy
  #   clear_header
  #   delete :destroy, id: @objects[0].id
  #   result = JSON.parse(@response.body)
  #   assert_error_unauthorized nil, result
  # end
  
  def test_index
    return if can_not_test? :index
    get :index, @authenticate
    result = JSON.parse(@response.body)
    assert result.has_key?("count"), "RESPONSE: " + result.to_s
    assert result.has_key?("data"), "RESPONSE: " + result.to_s
  end
  
  def test_index_without_authentication
    return if can_not_test? :index
    clear_header
    get :index
    result = JSON.parse(@response.body)
    assert_error_unauthorized nil, result
  end
  
  def test_index_limit
    return if can_not_test? :index
    get :index, make_values({limit: 1})
    result = JSON.parse(@response.body)
    assert result.has_key?("count"), "RESPONSE: " + result.to_s
    assert result.has_key?("data"), "RESPONSE: " + result.to_s
    assert_equal result["data"].count, 1, "RESPONSE: " + result.to_s
  end
  
  def test_index_limit_blank
    return if can_not_test? :index
    get :index, make_values({limit: ""})
    result = JSON.parse(@response.body)
    assert result.has_key?("count"), "RESPONSE: " + result.to_s
    assert result.has_key?("data"), "RESPONSE: " + result.to_s
  end
  
  def test_index_limit_invalid
    return if can_not_test? :index
    get :index, make_values({limit: "abc"})
    result = JSON.parse(@response.body)
    assert result.has_key?("count"), "RESPONSE: " + result.to_s
    assert result.has_key?("data"), "RESPONSE: " + result.to_s
  end
  
  def test_index_offset
    return if can_not_test? :index
    get :index, make_value({offset: 1})
    result = JSON.parse(@response.body)
    assert result.has_key?("count"), "RESPONSE: " + result.to_s
    assert result.has_key?("data"), "RESPONSE: " + result.to_s
  end
  
  # def test_index_offset_blank
  #   return if can_not_test? :index
  #   get :index, offset: ""
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  # end
  
  # def test_index_offset_invalid
  #   return if can_not_test? :index
  #   get :index, offset: "abc"
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  # end
  
  # def test_index_where
  #   return if can_not_test? :index
  #   for object in @objects
  #     get :index, where: {id: object.id}
  #     result = JSON.parse(@response.body)
  #     assert result.has_key?("results"), "RESPONSE: " + result.to_s
  #     assert_equal object.id, result["results"][0]["id"], "RESPONSE: " + result[0].to_s
  #   end
  # end
  
  # def test_index_where_blank
  #   return if can_not_test? :index
  #   get :index, where: ""
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  # end
  
  # def test_index_where_invalid
  #   return if can_not_test? :index
  #   get :index, where: "abc"
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  #   get :index, where: {a: "tt"}
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  # end
  
  # def test_index_order
  #   return if can_not_test? :index
  #   get :index, order: {id: :desc}
  #   result = JSON.parse(@response.body)
  #   assert result.has_key?("results"), "RESPONSE: " + result.to_s
  # end
  
  # def test_index_order_blank
  #   return if can_not_test? :index
  #   get :index, order: ""
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  # end
  
  # def test_index_order_invalid
  #   return if can_not_test? :index
  #   get :index, order: "aaaaa"
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  #   get :index, order: {a: "aa"}
  #   result = JSON.parse(@response.body)
  #   assert_error_badrequest nil, result
  # end
  
  # def test_view
  #   return if can_not_test? :view
  #   for object in @objects
  #     get :view, id: object.id
  #     result = JSON.parse(@response.body)
  #     assert_not result.has_key?("error"), "RESPONSE: " + result.to_s
  #     assert_equal object.id, result["id"], "RESPONSE: " + result.to_s
  #   end
  # end
  
  # def test_view_blank
  #   return if can_not_test? :view
  #   get :view, id: ""
  #   result = JSON.parse(@response.body)
  #   assert_error_notfound nil, result
  # end
  
  # def test_view_invalid
  #   return if can_not_test? :view
  #   get :view, id: "aaaaa"
  #   result = JSON.parse(@response.body)
  #   assert_error_notfound nil, result
  # end
  
  # def test_view_without_authentication
  #   return if can_not_test? :view
  #   clear_header
  #   get :view, id: @objects[0].id
  #   result = JSON.parse(@response.body)
  #   assert_error_unauthorized nil, result
  # end
  
end