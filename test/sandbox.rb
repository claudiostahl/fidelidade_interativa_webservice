require 'test_helper'

class Sandbox < ActionController::TestCase
  
  def test_default
    
    @data = {device: "device_root", token: "token_root", related: "product", related_id: products(:product1).id}
    @controller = Backend::TranslationController.new
    @data_put = @data.clone
    @data_put[:data] = {language: "pt_BR", code: "code", image: fixture_file_upload("images/photo.jpg", 'image/jpeg', :binary)}
    
    put :update, @data_put
    result = JSON.parse(@response.body)
    p result
    
    @data_put[:data].delete :image
    @data_put[:data][:delete] = ["image"]
    put :update, @data_put
    result = JSON.parse(@response.body)
    p result
    
    get :index, @data
    result = JSON.parse(@response.body)
    p result
    
  end
end