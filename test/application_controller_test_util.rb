require 'application_controller_test'

module ApplicationControllerTestUtil
  
  def inflate_data(id = 1, data={})
    data = data.merge @attribute_default
    for attribute in @attribute_string
      data[attribute] = attribute.to_s + id.to_s
    end
    for attribute in @attribute_small_string
      small_id = id/100000
      if small_id <= 0
        small_id = id
      end
      data[attribute] = "a" + small_id.to_s
    end
    for attribute in @attribute_url
      data[attribute] = "http://" + attribute.to_s + id.to_s + ".com"
    end
    for attribute in @attribute_email
      data[attribute] = attribute.to_s + id.to_s + "@teste.com"
    end
    for attribute in @attribute_boolean
      data[attribute] = true
    end
    for attribute in @attribute_integer
      data[attribute] = 1
    end
    for attribute in @attribute_number
      data[attribute] = 20.50
    end
    for attribute in @attribute_date
      data[attribute] = "2015-11-12"
    end
    for attribute in @attribute_datetime
      data[attribute] = "2015-11-11 10:30:30"
    end
    for attribute in @attribute_time
      data[attribute] = "10:30:30"
    end
    for attribute in @attribute_image
      data[attribute] = fixture_file_upload("images/photo.jpg", 'image/jpeg', :binary)
    end
    if @related
      data[:related] = @related.id
      # data[:related_id] = @related.id
      # data[:related_type] = @related.class
    end
    data = hash_symbol_to_string data
    data
  end
  
  def array_symbol_to_string(data)
    output = []
    for key in data
      if key.is_a? Symbol 
        output << key.to_s
      elsif key.is_a? String
        output << key
      end
    end
    output
  end
  
  def hash_symbol_to_string(data)
    output = {}
    data.each do |key, value|
      if key.is_a? Symbol 
        output[key.to_s] = value
      elsif key.is_a? String
        output[key] = value
      end
    end
    output
  end
  
end