require 'application_controller_test'

module ApplicationControllerTestAdvanced
  
  # def test_password
  #   if can_test? :create
  #     i=0
  #     for attribute in @attribute_password
  #       password = "12345678"
  #       data = inflate_data i+=1
  #       data[attribute] = password
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result
  #       id = result["success"]["id"]
  #       entity = @model_class.find(id)
  #       assert entity.is_password? password
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_password
  #       password = "12345678"
  #       data = @objects[0].attributes
  #       data[attribute] = password
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result
  #       id = result["success"]["id"]
  #       entity = @model_class.find(id)
  #       assert entity.is_password? password
  #     end
  #   end
  # end
  
  # def test_has_one
  #   i = 0
  #   if can_test? :create
  #     @has_one.each do |key, value|
  #       data = inflate_data i+=1
  #       data[key] = value
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, key
  #       model = @model_class.find result["success"]["id"]
  #       assert model
  #       assert_equal result["success"][key], value
  #       data = inflate_data i+=1
  #       data[key] = "h"
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, key
  #     end
  #   end
  #   if can_test? :update
  #     @has_one.each do |key, value|
  #       data = @objects[0].attributes
  #       data[key] = value
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, key
  #       model = @model_class.find result["success"]["id"]
  #       assert model
  #       assert_equal result["success"][key], value
  #       data = @objects[0].attributes
  #       data[key] = "h"
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, key
  #     end
  #   end
  # end
  
  # def test_has_many
  #   i = 0
  #   if can_test? :create
  #     @has_many.each do |key, value|
  #       data = inflate_data i+=1
  #       data[key] = value
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, key
  #       model = @model_class.find result["success"]["id"]
  #       many = model.builder_json_by_array[key]
  #       for item in value
  #         assert many.include? item
  #       end
  #       data = inflate_data i+=1
  #       data[key] = "h"
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, key
  #       data = inflate_data i+=1
  #       data[key] = ["h", "i"]
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, key
  #     end
  #   end
  #   if can_test? :update
  #     @has_many.each do |key, value|
  #       data = @objects[0].attributes
  #       data[key] = value
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result
  #       model = @model_class.find result["success"]["id"]
  #       many = model.builder_json_by_array[key]
  #       for item in value
  #         assert many.include? item
  #       end
  #       data = @objects[0].attributes
  #       data[key] = "h"
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result
  #       data = @objects[0].attributes
  #       data[key] = ["h", "i"]
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result
  #     end
  #   end
  # end
  
  # def test_max_lenght
  #   if can_test? :create
  #     i=0
  #     @max_lenght.each do |key, value|
  #       data = inflate_data i+=1
  #       if @attribute_string.include?(key) || @attribute_small_string.include?(key)
  #         data[key] = "A" * value + "B"
  #       end
  #       if @attribute_email.include? key
  #         data[key] = "A" * value + "@a.com"
  #       end
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, key
  #       data = inflate_data i+=1
  #       if @attribute_string.include? key
  #         data[key] = "A" * (value-1)
  #       end
  #       if @attribute_email.include? key
  #         data[key] = "A" * (value-7) + "@a.com"
  #       end
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, key
  #     end
  #   end
  #   if can_test? :update
  #     @max_lenght.each do |key, value|
  #       data = @objects[0].attributes
  #       if @attribute_string.include?(key) || @attribute_small_string.include?(key)
  #         data[key] = "C" * value + "B"
  #       end
  #       if @attribute_email.include? key
  #         data[key] = "C" * value + "@a.com"
  #       end
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result
  #       data = @objects[0].attributes
  #       if @attribute_string.include? key
  #         data[key] = "C" * (value-1)
  #       end
  #       if @attribute_email.include? key
  #         data[key] = "C" * (value-7) + "@a.com"
  #       end
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result
  #     end
  #   end
  # end
  
  # def test_required
  #   if can_test? :create
  #     i=0
  #     for attribute in @is_required
  #       data = inflate_data i+=1
  #       data[attribute] = nil
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @is_required
  #       data = @objects[0].attributes
  #       data[attribute] = nil
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  # end
  
  # def test_not_required
  #   if can_test? :create
  #     i=0
  #     for attribute in @not_required
  #       data = inflate_data i+=1
  #       data[attribute] = nil
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @not_required
  #       data = @objects[0].attributes
  #       data[attribute] = nil
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, attribute
  #     end
  #   end
  # end
    
  # def test_unique
  #   if can_test? :create
  #     for attribute in @is_unique
  #       object = @objects[0]
  #       id = 1
  #       data = inflate_data id
  #       data[attribute] = object.attributes[attribute]
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @is_unique
  #       data = @objects[0].attributes
  #       data[attribute] = @objects[1].attributes[attribute]
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  # end
  
  # def test_unique_company
  #   if can_test? :create
  #     i=0
  #     for attribute in @is_unique_company
  #       object = @objects[0]
  #       config_backend
  #       data = inflate_data i+=1
  #       data[attribute] = object.attributes[attribute]
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #       config_backend_other_company
  #       data = inflate_data i+=1
  #       data["company"] = companies(:company2).id
  #       data[attribute] = object.attributes[attribute]
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, attribute
  #       assert_find_data data, attribute, "company"
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @is_unique_company
  #       object = @objects[0]
  #       object2 = @objects[1]
  #       config_backend
  #       data = object.attributes
  #       data[attribute] = object2.attributes[attribute]
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #       config_backend_other_company
  #       data = object.attributes
  #       data["company"] = companies(:company2).id
  #       data[attribute] = object2.attributes[attribute]
  #       put :update, data: data, id: object2.id
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, attribute
  #       assert_find_data data, attribute, "company"
  #     end
  #   end
  # end
  
  # def test_email
  #   if can_test? :create
  #     for attribute in @attribute_email
  #       data = inflate_data
  #       data[attribute] = "email"
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_email
  #       data = @objects[0].attributes
  #       data[attribute] = "email"
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  # end
  
  # def test_url
  #   if can_test? :create
  #     for attribute in @attribute_url
  #       data = inflate_data
  #       data[attribute] = "url"
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_url
  #       data = @objects[0].attributes
  #       data[attribute] = "url"
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  # end
  
  # def test_boolean
  #   if can_test? :create
  #     i = 1
  #     for attribute in @attribute_boolean
  #       data = inflate_data i
  #       i+=1
  #       data[attribute] = true
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, attribute
  #       data = inflate_data i
  #       i+=1
  #       data[attribute] = false
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_boolean
  #       data = @objects[0].attributes
  #       data[attribute] = true
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, attribute
  #       data = @objects[0].attributes
  #       data[attribute] = false
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, attribute
  #     end
  #   end
  # end
  
  # def test_integer
  #   if can_test? :create
  #     for attribute in @attribute_integer
  #       data = inflate_data
  #       data[attribute] = 10.5
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_integer
  #       data = @objects[0].attributes
  #       data[attribute] = 10.5
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  # end
  
  # def test_number
  #   if can_test? :create
  #     for attribute in @attribute_number
  #       data = inflate_data
  #       data[attribute] = "test"
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_number
  #       data = @objects[0].attributes
  #       data[attribute] = "test"
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  # end
  
  # def test_date
  #   if can_test? :create
  #     for attribute in @attribute_date
  #       data = inflate_data
  #       data[attribute] = "test"
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_date
  #       data = @objects[0].attributes
  #       data[attribute] = "test"
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  # end
  
  # def test_datetime
  #   if can_test? :create
  #     for attribute in @attribute_datetime
  #       data = inflate_data
  #       data[attribute] = "2015-11-26T10:00:00+03:00"
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result
  #       assert_equal result["success"][attribute], "2015-11-26T07:00:00+00:00"
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_datetime
  #       data = @objects[0].attributes
  #       data[attribute] = "2015-11-26T10:00:00+03:00"
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result
  #       assert_equal result["success"][attribute], "2015-11-26T07:00:00+00:00"
  #     end
  #   end
  # end
  
  # def test_datetime_invalid
  #   if can_test? :create
  #     for attribute in @attribute_datetime
  #       data = inflate_data
  #       data[attribute] = "test"
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  #   if can_test? :update
  #     for attribute in @attribute_datetime
  #       data = @objects[0].attributes
  #       data[attribute] = "test"
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_error_invalid data, result, attribute
  #     end
  #   end
  # end
   
  # def test_unallowed
  #   if can_test? :create
  #     @unallowed.each do |key, value|
  #       data = inflate_data
  #       data[key] = value
  #       post :create, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, key
  #       id = result["success"]["id"]
  #       model = @model_class.find id
  #       if (model.attributes[key])
  #         assert_not_equal model.attributes[key], value
  #       end
  #     end
  #   end
  #   if can_test? :update
  #     @unallowed.each do |key, value|
  #       data = @objects[0].attributes
  #       data[key] = value
  #       put :update, data: data
  #       result = JSON.parse(@response.body)
  #       assert_success data, result, key
  #       id = result["success"]["id"]
  #       model = @model_class.find id
  #       if (model.attributes[key])
  #         assert_not_equal model.attributes[key], value
  #       end
  #     end
  #   end
  # end
    
end