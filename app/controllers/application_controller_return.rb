module ApplicationControllerReturn
  
  def render_json(result)
    begin
      status = :success
      if result.is_a? Hash
        if result.has_key?(:error)
          status = result[:error][:code]
        end
      elsif result.is_a? Array
        for row in result
          if row.has_key?(:error)
            status = row[:error][:code]
          end
        end
      end
      case status
        when :bad_request
          http_status = 400
        when :unauthorized
          http_status = 401
        when :invalid
          http_status = 400
        when :not_found
          http_status = 404
        when :success
          http_status = 200
        else
          http_status = 500
      end
      render json: result, status: http_status
    rescue => e
      render json: return_internal_server_error(e), status: 500
    end
  end

  def return_destroyed
    {:success => {:destroyed_at => Time.new}}
  end

  def return_updated(result)
    {:success => result}
  end

  def return_created(result)
    {:success => result}
  end

  def return_ok()
    {:success => true}
  end

  def return_result(result)
    result
  end

  def return_bad_request(e)
    {:error => {:code => :bad_request, :message => e.message}}
  end
  
  def return_unauthorized(e)
    {:error => {:code => :unauthorized, :message => e.message}}
  end
  
  def return_invalid(e)
    {:error => {:code => :invalid, :message => e.message}}
  end
  
  def return_invalid_range(e)
    {:error => {:code => :invalid, :message => "Range Invalid"}}
  end
  
  def return_not_found(e)
    {:error => {:code => :not_found, :message => "Not Found"}}
  end
  
  def return_internal_server_error(e)
    # log exception
    debug = []
    debug << e.class.name
    debug << e.message
    i=0
    for row in e.backtrace
      debug << row
      i+=1
      if i==20
        break
      end
    end
    # debug = nil
    {:error => {:code => :internal_server_error, :message => "Internal Server Error", :debug => debug}}
  end

end