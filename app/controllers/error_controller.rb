class ErrorsController < ActionController::Base
  def not_found
    render :json => {:error => {:code => :not_found, :message => "Not Found"}}.to_json, :status => 404
  end

  def exception
    render :json => {:error => {:code => :internal_server_error, :message => "Internal Server Error"}}.to_json, :status => 500
  end
end