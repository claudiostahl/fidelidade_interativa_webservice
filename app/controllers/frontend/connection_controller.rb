module Frontend
  class ConnectionController < ApplicationController

    def login
      result = process_one do
        if (!params.has_key?("company") || !params.has_key?("login") || !params.has_key?("password"))
          return render :json => {:error => {:code => 400, :message => "Invalid data"}}, status: :bad_request
        end
        field_company = params["company"]
        field_login = params["login"]
        field_password = params["password"]
        company = Company.where(code: field_company).first
        user = User.where(email: field_login).first
        if (!company || !user || !user.is_password?(field_password) || (user.company.id != company.id))
          return render :json => {:error => {:code => 400, :message => "Invalid credentials"}}, status: :bad_request
        end
        if (!administrator.enabled?)
          return render :json => {:error => {:code => 401, :message => "Disabled user"}}, status: :unauthorized
        end
        if !company.enabled_frontend?
          raise UnauthorizedException, "Frontend disabled"
        end
        connection = Connection.build_default(company)
        user.connections.push(connection)
        user.save
        connection.builder_json_by_array([:device, :token, :created_at])
      end
      render_json result
    end

    def logout
      result = process_one do
        authenticate_backend
        @connection.destroy!
        return_ok
      end
      render_json result
    end

  end
end