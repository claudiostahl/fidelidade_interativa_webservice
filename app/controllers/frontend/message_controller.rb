module Frontend
  class MessageController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = Message
      @restful_attributes = [:id, :company, :message_dispatches, :name, :description]
    end

  end
end