module Frontend
  class AccessController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = Access
      @restful_attributes = [:id, :company, :user, :related_id, :related_type]
      @restful_permits = [:related_id, :related_type]
    end

  end
end