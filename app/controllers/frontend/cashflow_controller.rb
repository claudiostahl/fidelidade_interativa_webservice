module Frontend
  class CashflowController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = Cashflow
      @restful_attributes = [:id, :company, :user, :related_id, :related_type, :type_flow, :value]
    end

  end
end