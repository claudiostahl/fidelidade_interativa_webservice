module Frontend
  class PostController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = Post
      @restful_attributes = [:id, :company, :title, :slug, :description, :content, :publication_date, :enabled]
      @restful_constants = {company: @company}
      @restful_filters = {company: @company}
    end

  end
end