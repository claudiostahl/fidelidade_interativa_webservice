module Frontend
  class RecoverController < ApplicationController

    def create
      result = process_one do
        login = params[:login]
        user = User.where(email: login).first
        if !user
          raise BadRequestException, "Invalid email"
        end
        user.create_recover
        user.save!
        return_ok
      end
      render_json result
    end
    
    def update
      result = process_one do
        token = params[:token]
        password = params[:password]
        user = User.where(recover_token: token).first
        if !user
          raise BadRequestException, "Invalid token"
        end
        user.password = password
        user.save!
        return_ok
      end
      render_json result
    end

  end
end