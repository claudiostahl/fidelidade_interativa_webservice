module Frontend
  class ContentController < CrudController

    def index
      result = process_one do
        output = {}
        configs = Config.where(company: @company)
        for config in configs
          value = nil
          if !config.value_text.blank?
            value = config.value_text
          elsif !config.value_number.blank?
            value = config.value_number
          else
            value = config.value_string
          end
          output[config.code] = value
        end
        output
      end
      render_json result
    end

  end
end