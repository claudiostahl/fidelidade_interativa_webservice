module Frontend
  class SharedController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = Shared
      @restful_attributes = [:id, :company, :user, :related_id, :related_type, :token]
      @restful_permits = [:related_id, :related_type]
    end

  end
end