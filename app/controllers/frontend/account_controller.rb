module Frontend
  class AccountController < ApplicationController
    
    def init_update
      authenticate_backend
      check_password
    end

    def update
      result = process_one(:init_update) do
        attributes = [:id, :company, :token, :name, :email, :language, :credit, :can_notification, :last_access_at, :last_buy_at, :enabled]
        permits = [:id, :name, :email, :language, :can_notification]
        data = params.require(:data)
        model = populate_model @user, data, permits
        if !model.valid?
          return return_invalid model.errors.full_messages.first
        end
        model.save!
        return_updated model.builder_json_by_array(attributes)
      end
      render_json result
    end

  end
end