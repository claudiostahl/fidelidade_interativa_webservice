module Frontend
  class OrderController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = Order
      @restful_attributes = [:id, :company, :user, :administrator, :reward, :token, :comments, :reward_name, :reward_slug, :reward_short_description, :reward_description, :reward_rule_description, :value, :price]
      @restful_permits = [:user, :reward, :comments]
    end
    
    def parse_create(data)
      if data.has_key?("user")
        data["user"] = convert_to_id data["user"], User
      end
      if data.has_key?("reward")
        data["reward"] = convert_to_id data["reward"], Reward
      end
      data
    end
    
    def before_save(model)
      model.validate_reward!
      model
    end

  end
end