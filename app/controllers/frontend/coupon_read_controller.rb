module Frontend
  class CouponReadController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = CouponRead
      @restful_attributes = [:id, :company, :coupon_group, :coupon, :user, :value]
      @restful_permits = [:coupon]
    end
    
    def parse_create(data)
      if data.has_key?("coupon")
        data["coupon"] = convert_to data["coupon"], :token, Coupon
      end
      data
    end

  end
end