module Frontend
  class VoteController < CrudController
    
    def prepare_default
      @related = params.require(:related)
      case @related
        when "product"
          @related_type = Product
          @package = "product"
          @resource = "product"
        when "reward"
          @related_type = Reward
          @package = "reward"
          @resource = "reward"
        else
          raise NotFoundException
      end
      authenticate_frontend
      @restful_model_class = Vote
      @restful_attributes = [:id, :user, :company, :related, :related_type, :vote]
      @restful_permits = [:related, :related_type, :vote]
    end
    
    def update
      result = process_data(:prepare_default) do |data|
        apply_config data
      end
      render_json result
    end
    
    def apply_config(values)
      related_id = values["related_id"]
      related = @related_type.where(id: related_id.to_s).first
      if !related
        raise BadRequestException, "Invalid related"
      end
      vote = values["vote"]
      entity = Vote.where(related: related, related_type: @related_type.to_s, user: @user, company: @company).first
      if entity
        data = entity.attributes
        data["vote"] = vote
        update_default @restful_model_class, entity.id, data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
      else
        data = {company: @company, user: @user, related: related, related_type: @related_type.to_s, vote: vote}
        create_default @restful_model_class, data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
      end
    end

  end
end