module Frontend
  class UserController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = User
      @restful_attributes = [:id, :company, :token, :name, :email, :facebook, :recover_token, :credit, :can_notification, :last_access_at, :last_buy_at, :enabled]
      @restful_permits = [:name, :email, :password, :facebook, :can_notification]
      @restful_constants = {company: @company}
      @restful_filters = {company: @company}
    end

  end
end