module Frontend
  class NotificationController < CrudController

    def prepare_default
      authenticate_frontend
      @restful_model_class = Notification
      @restful_attributes = [:id, :company, :user, :name, :description, :read, :arquived]
      @restful_permits = [:read, :arquived]
    end

  end
end