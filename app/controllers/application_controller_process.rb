module ApplicationControllerProcess
  
  def process_one(prepare_method=nil, &block)
    safe_process do
      if prepare_method != nil && self.respond_to?(prepare_method)
        self.send(prepare_method)
      end
      output = {}
      ActiveRecord::Base.transaction do
        output = safe_process do
          block.call
        end
        if output != nil && output.has_key?(:error)
          raise ActiveRecord::Rollback
        end
      end
      output
    end
  end
  
  def process_one_data(prepare_method=nil, &block)
    safe_process do
      if prepare_method != nil && self.respond_to?(prepare_method)
        self.send(prepare_method)
      end
      output = {}
      data = params.require(:data)
      delete_data data
      ActiveRecord::Base.transaction do
        output = safe_process do
          block.call data
        end
        if output != nil && output.has_key?(:error)
          raise ActiveRecord::Rollback
        end
      end
      output
    end
  end

  def process_data(prepare_method=nil, &block)
    safe_process do
      if prepare_method != nil && self.respond_to?(prepare_method)
        self.send(prepare_method)
      end
      output = {}
      data = params.require(:data)
      ActiveRecord::Base.transaction do
        if data.is_a?(Hash) && data.has_key?("0")
          output = []
          rollback = false;
          data.each do |index, d|
            delete_data d
            line = safe_process do
              block.call d
            end
            output << line
            if line != nil && line.has_key?(:error)
              rollback = true
            end
          end
          if rollback
            raise ActiveRecord::Rollback
          end
        elsif data.is_a?(Hash)
          delete_data data
          output = safe_process do
            block.call data
          end
          if output != nil && output.has_key?(:error)
            raise ActiveRecord::Rollback
          end
        elsif data.is_a?(Array)
          output = []
          rollback = false;
          for d in data
            delete_data d
            line = safe_process do
              block.call d
            end
            output << line
            if line != nil && line.has_key?(:error)
              rollback = true
            end
          end
          if rollback
            raise ActiveRecord::Rollback
          end
        else
          raise BadRequestException, "Invalid data"
        end
      end
      output
    end
  end
  
  def delete_data(data)
    if data.has_key?(:delete)
      if data[:delete].is_a?(Hash) && data[:delete].has_key?("0")
        data[:delete].each do |index, d|
          data[d] = nil
        end
      elsif data[:delete].is_a?(Array)
        for d in data[:delete]
          data[d] = nil
        end
      end
      data.delete :delete
    end
    data
  end
  
  def safe_process
    begin
      yield
      
    rescue ActiveRecord::DeleteRestrictionError => e
      return_bad_request e
    rescue ActionController::ParameterMissing => e
      return_bad_request e
    rescue ActiveRecord::RecordInvalid => e
      return_invalid e
    rescue RangeError => e
      return_invalid_range e
    rescue InvalidException => e
      return_invalid e
    rescue UnauthorizedException => e
      return_unauthorized e
    rescue NotFoundException => e
      return_not_found e
    rescue BadRequestException => e
      return_bad_request e
    rescue Exception => e
      return_internal_server_error e
    end
  end

end