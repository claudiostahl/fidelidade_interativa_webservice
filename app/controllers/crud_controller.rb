class CrudController < ApplicationController
  
  def init
    if self.respond_to? :prepare_default
      prepare_default
    end
  end
  
  def init_index
    if params.has_key?(:where) && !params[:where].blank?
      @restful_where = params[:where]
    end
    if params.has_key?(:order) && !params[:order].blank?
      @restful_order = params[:order]
    end
    if params.has_key?(:limit) && !params[:limit].blank? && params[:limit].is_integer?
      @restful_limit = params[:limit]
    end
    if params.has_key?(:offset) && !params[:offset].blank? && params[:offset].is_integer?
      @restful_offset = params[:offset]
    end
    if params.has_key?(:page) && !params[:page].blank? && params[:page].is_integer?
      @restful_offset = (params[:page].to_i - 1) * @restful_limit.to_i
    end
    init
    if self.respond_to? :prepare_index_and_view
      prepare_index_and_view
    end
    if self.respond_to? :prepare_index
      prepare_index
    end
  end
  
  def init_view
    init
    if self.respond_to? :prepare_index_and_view
      prepare_index_and_view
    end
    if self.respond_to? :prepare_view
      prepare_view
    end
  end
  
  def init_create
    init
    if self.respond_to? :prepare_create_and_update
      prepare_create_and_update
    end
    if self.respond_to? :prepare_create
      prepare_create
    end
  end
  
  def init_update
    init
    if self.respond_to? :prepare_create_and_update
      prepare_create_and_update
    end
    if self.respond_to? :prepare_update
      prepare_update
    end
  end
  
  def init_destroy
    init
    if self.respond_to? :prepare_destroy
      prepare_destroy
    end
  end

  def index
    result = process_one(:init_index) do
      index_default @restful_model_class, @restful_attributes, @restful_filters, @restful_where, @restful_order, @restful_limit, @restful_offset
    end
    render_json result
  end

  def view
    result = process_one(:init_view) do
      view_default @restful_model_class, params[:id], @restful_attributes, @restful_filters
    end
    render_json result
  end

  def create
    result = process_data(:init_create) do |data|
      create_default @restful_model_class, data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
    end
    render_json result
  end

  def update
    if params.has_key? :id
      result = process_one_data(:init_update) do |data|
        update_default @restful_model_class, params[:id], data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
      end
    else
      result = process_data(:init_update) do |data|
        update_default @restful_model_class, data["id"], data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
      end
    end
    render_json result
  end

  def destroy
    # render_json params.to_s
    
    if params.has_key? :id
      result = process_one(:init_destroy) do
        destroy_default @restful_model_class, params[:id], @restful_filters
      end
    else
      result = process_data(:init_destroy) do |data|
        destroy_default @restful_model_class, data, @restful_filters
      end
    end
    render_json result
  end

end