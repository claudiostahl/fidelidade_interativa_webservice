module Api
  class PremiumController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "premium"
      authenticate_api
      @restful_model_class = Premium
      @restful_attributes = {id: :id, company: :company, user: :user_token, premium_type: :premium_type_token, developer: :developer, campaign: :campaign, premium_type_name: :premium_type_name, premium_type_description: :premium_type_description, premium_type_notification_title: :premium_type_notification_title, premium_type_notification_description: :premium_type_notification_description, value: :value}
      @restful_permits = [:user, :premium_type, :campaign, :value]
      @restful_constants[:developer] = @developer
    end
    
    def parse_create(data)
      if data.has_key?("user")
        data["user"] = convert_to data["user"], :token, User
      end
      if data.has_key?("premium_type")
        data["premium_type"] = convert_to data["premium_type"], :token, PremiumType
      end
      data
    end
    
    def before_save(model)
      model.validate_limit!
      model
    end

  end
end