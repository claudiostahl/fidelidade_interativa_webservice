module Api
  class ProductController < CrudController

    def prepare_default
      @package = "product"
      @resource = "product"
      authenticate_api
      @restful_model_class = Product
      @restful_attributes = [:company, :related_products, :id, :name, :slug, :short_description, :description, :price, :special_price, :first_vote, :rate, :enabled]
    end

  end
end