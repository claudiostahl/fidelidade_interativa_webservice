module Api
  class RewardController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "reward"
      authenticate_api
      @restful_model_class = Reward
      @restful_attributes = [:id, :company, :name, :slug, :short_description, :description, :rule_description, :value, :price, :first_vote, :publication_date, :limit, :limit_user, :enabled, :arquived]
    end

  end
end