module Api
  class UserController < CrudController

    def prepare_default
      @package = "default"
      @resource = "user"
      authenticate_api
      @restful_model_class = User
      @restful_attributes = [:id, :company, :token, :name, :email, :facebook, :recover_token, :credit, :can_notification, :last_access_at, :last_buy_at, :enabled]
    end

  end
end