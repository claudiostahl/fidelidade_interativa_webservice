module Api
  class PostController < CrudController

    def prepare_default
      @package = "post"
      @resource = "post"
      authenticate_api
      @restful_model_class = Post
      @restful_attributes = [:id, :company, :title, :slug, :description, :content, :publication_date, :enabled]
    end

  end
end