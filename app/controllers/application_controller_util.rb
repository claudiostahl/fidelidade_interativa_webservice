module ApplicationControllerUtil
  
  def populate_model(model, data, permits=[], constants={})
    if !data.is_a? Hash
      raise BadRequestException, "Invalid data"
    end
    data = hash_symbol_to_string data
    if self.respond_to? :parse_create_and_update
      data = parse_create_and_update data
    end
    if self.respond_to? :parse_create_and_update_constants
      constants = parse_create_and_update_constants model, constants, data
    end
    attributes = data
    if (permits.count() > 0)
      attributes = {}
      for permit in permits
        if data.has_key?(permit.to_s)
          attributes[permit.to_s] = data[permit.to_s]
        end
      end
    end
    if (constants.count() > 0)
      constants.each do |key, value|
        attributes[key] = value
      end
    end
    model.assign_attributes(attributes)
    model
  end
  
  def check_filters(model, filters)
    filters.each do |key, value|
      model_value = model.send(key)
      is_collection = model_value.kind_of?(ActiveRecord::Associations::CollectionProxy)
      if (is_collection && !model_value.include?(value)) || (!is_collection && model_value != value)
        raise UnauthorizedException, "Unauthorized change field"
      end
    end
  end
  
  def convert_to(value, attribute, modelClass)
    if value == nil
      return nil
    end
    model = modelClass.where(attribute => value).first
    if !model
      raise InvalidException, "Invalid relation"
    end
    model
  end
  
  def convert_to_id(id, modelClass)
    if id == nil
      return nil
    end
    model = modelClass.where(id: id).first
    if !model
      raise InvalidException, "Invalid relation"
    end
    model
  end
  
  def convert_array_to(data, attribute, modelClass)
    if data == nil
      return []
    end
    output = []
    if (data.kind_of?(Array) && data.count)
      for value in data
        model = modelClass.where(attribute => value).first
        if !model
          raise InvalidException, "Invalid relation"
        end
        output << model
      end
    elsif (data.kind_of?(Hash) && data.count)
      data.each do |key, value|
        model = modelClass.where(attribute => value).first
        if !model
          raise InvalidException, "Invalid relation"
        end
        output << model
      end
    else
      raise InvalidException, "Invalid relation" 
    end
    return output
  end

  def convert_array_to_ids(data, modelClass)
    if data == nil
      return []
    end
    output = []
    if (data.kind_of?(Array) && data.count)
      for string_id in data
        model = modelClass.where(id: string_id).first
        if !model
          raise InvalidException, "Invalid relation"
        end
        output << model
      end
    elsif (data.kind_of?(Hash) && data.count)
      data.each do |key, string_id|
        model = modelClass.where(id: string_id).first
        if !model
          raise InvalidException, "Invalid relation"
        end
        output << model
      end
    else
      raise InvalidException, "Invalid relation"
    end
    return output
  end

  def convert_array_and_hash_to_ids(data, modelClass, attribute)
    if data == nil
      return []
    end
    output = []
    if (data.kind_of?(Array) && data.count)
      for hash in data
        string_id = hash[attribute.to_s]
        m = modelClass.where(id: string_id).first
        if !m
          raise InvalidException, "Invalid relation"
        end
        hash[attribute.to_s] = m
        output << hash
      end
    else
      raise InvalidException, "Invalid relation"
    end
    return output
  end
  
  def hash_symbol_to_string(data)
    output = {}
    data.each do |key, value|
      if key.is_a? Symbol 
        output[key.to_s] = value
      elsif key.is_a? String
        output[key] = value
      end
    end
    output
  end
  
  def hash_string_to_symbol(data)
    output = {}
    data.each do |key, value|
      if key.is_a? Symbol 
        output[key] = value
      elsif key.is_a? String
        output[key.parameterize.underscore.to_sym] = value
      end
    end
    output
  end

end