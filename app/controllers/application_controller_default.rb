module ApplicationControllerDefault

  def index_default(modelClass, attributes=[], filters={}, where={}, order={}, limit=1000, offset=0)
    if !limit.is_integer?
      raise BadRequestException, "Invalid Limit"
    end
    if !offset.is_integer?
      raise BadRequestException, "Invalid Offset"
    end
    if !where.is_a? Hash
      raise BadRequestException, "Invalid Where"
    end
    if order.is_a? String
      if order[0] == "-"
        order[0] = '' 
        order = {order => :desc}
      else
        order = {order => :asc}
      end
    end
    if !order.is_a? Hash
      raise BadRequestException, "Invalid Order"
    end
    total = 0
    if (filters.count == 0 && where.count == 0)
      models = modelClass.all
    elsif where.has_key?("filter")
      filter_where = []
      filter_params = []
      where[:filter].each do |name, itens|
        if itens.has_key?(:id)
          filter_where << "(#{name}_id = ?)"
          filter_params << itens[:id]
        end
        if itens.has_key?(:like)
          filter_where << "(#{name} ILIKE ?)"
          filter_params << "%" + itens[:like] + "%"
        end
        if itens.has_key?(:value)
          filter_where << "(#{name} = ?)"
          filter_params << itens[:value]
        end
        if itens.has_key?(:number_start) && itens.has_key?(:number_end)
          filter_where << "(#{name} >= ? and #{name} <= ?)"
          filter_params << itens[:number_start]
          filter_params << itens[:number_end]
        elsif itens.has_key?(:number_start)
          filter_where << "(#{name} >= ?)"
          filter_params << itens[:number_start]
        elsif itens.has_key?(:number_end)
          filter_where << "(#{name} <= ?)"
          filter_params << itens[:number_end]
        end
        if itens.has_key?(:date_start) && itens.has_key?(:date_end)
          filter_where << "(#{name} >= ? and #{name} <= ?)"
          filter_params << DateTime.parse(itens[:date_start])
          filter_params << DateTime.parse(itens[:date_end])
        elsif itens.has_key?(:date_start)
          filter_where << "(#{name} >= ?)"
          filter_params << DateTime.parse(itens[:date_start])
        elsif itens.has_key?(:date_end)
          filter_where << "(#{name} <= ?)"
          filter_params << DateTime.parse(itens[:date_end])
        end
      end
      where_sql = filter_where.join " and "
      where_filter = [where_sql] + filter_params
      filters = hash_string_to_symbol filters
      models = modelClass.where(where_filter).where(filters)
    else
      constraints_all = where.merge filters
      constraints_all = hash_string_to_symbol constraints_all
      models = modelClass.where constraints_all
    end
    total = models.count() rescue raise(BadRequestException, "Invalid Query")
    if (order.count > 0)
      models = models.order(order) rescue raise(BadRequestException, "Invalid Order")
    end
    if (limit != nil)
      models = models.limit limit
    end
    if (offset != nil)
      models = models.offset offset
    end
    data = []
    total_return = models.count() rescue raise(BadRequestException, "Invalid Query")
    if (total_return > 0)
      for m in models
        data << m.builder_json_by_array(attributes)
      end
    end
    result = {count: total, data: data}
    return_result result
  end

  def view_default(modelClass, id, attributes=[], filters={})
    model = modelClass.where(id: id.to_s).first
    if !model
      raise NotFoundException
    end
    check_filters model, filters
    return_result model.builder_json_by_array(attributes)
  end

  def create_default(modelClass, data, attributes=[], permits=[], constants={}, filters={})
    if !data.is_a? Hash
      raise BadRequestException, "Invalid data"
    end
    model = modelClass.new
    if self.respond_to? :parse_create
      data = parse_create data
    end
    if self.respond_to? :parse_create_constants
      constants = parse_create_constants model, constants, data
    end
    data.delete "id"
    model = populate_model model, data, permits, constants
    check_filters model, filters
    if self.respond_to? :before_save
      model = before_save model
    end
    model.save!
    return_created model.builder_json_by_array(attributes)
  end

  def update_default(modelClass, id, data, attributes=[], permits=[], constants={}, filters={})
    if !data.is_a? Hash
      raise BadRequestException, "Invalid data"
    end
    model = modelClass.where(id: id.to_s).first
    if !model
      raise NotFoundException
    end
    data.delete "id"
    if self.respond_to? :parse_update
      data = parse_update data
    end
    if self.respond_to? :parse_update_constants
      constants = parse_update_constants model, constants, data
    end
    model = populate_model model, data, permits, constants
    check_filters model, filters
    if self.respond_to? :before_save
      model = before_save model
    end
    model.save!
    return_updated model.builder_json_by_array(attributes)
  end

  def destroy_default(modelClass, id, filters={})
    model = modelClass.where(id: id.to_s).first
    if !model
      raise NotFoundException
    end
    check_filters model, filters
    model.destroy!
    return_destroyed
  end

end