module Backend
  class AdministratorController < CrudController

    def prepare_default
      @package = "default"
      @resource = "administrator"
      authenticate_backend
      @restful_model_class = Administrator
      @restful_attributes = [:id, :companies, :resources, :language, :name, :email, :is_root, :enabled, :timezone, :created_at, :updated_at]
      @restful_permits = [:companies, :resources, :language, :name, :email, :password, :enabled, :timezone]
      @restful_constants = {companies: [@company]}
      if @administrator.is_root?
        @restful_filters = {}
        @restful_constants = {}
      end
    end
    
    def prepare_create_and_update
      if @administrator.is_root?
        @restful_permits << :is_root
      end
    end
    
    def prepare_update
      check_password
    end
    
    def prepare_destroy
      check_password
    end

    def parse_create_and_update(data)
      if data.has_key?("companies")
        data["companies"] = convert_array_to_ids data["companies"], Company
      end
      if data.has_key?("resources")
        data["resources"] = convert_array_to data["resources"], :code, Resource
      end
      if data.has_key?("language")
        data["language"] = convert_to_id data["language"], Language
      end
      data
    end

  end
end