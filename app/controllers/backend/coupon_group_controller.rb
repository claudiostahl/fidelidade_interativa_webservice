module Backend
  class CouponGroupController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "coupon_group"
      authenticate_backend
      @restful_model_class = CouponGroup
      @restful_attributes = [:id, :company, :administrator, :name, :description, :value, :validity, :limit, :order, :enabled, :created_at, :updated_at]
      @restful_permits = [:name, :description, :value, :validity, :limit, :order, :enabled]
      @restful_constants[:administrator] = @administrator
    end

  end
end