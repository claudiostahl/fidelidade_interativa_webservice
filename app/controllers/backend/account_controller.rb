module Backend
  class AccountController < ApplicationController
    
    def init_update
      @package = "default"
      @resource = "default"
      authenticate_backend
      check_password
    end

    def update
      result = process_one(:init_update) do
        attributes = [:id, :name, :email, :companies, :is_root, :enabled, :created_at, :updated_at]
        permits = [:name, :email, :password]
        data = params.require(:data)
        model = populate_model @administrator, data, permits
        model.save!
        return_updated model.builder_json_by_array(attributes)
      end
      render_json result
    end

  end
end