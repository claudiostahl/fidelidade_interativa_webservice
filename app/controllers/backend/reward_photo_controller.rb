module Backend
  class RewardPhotoController < PhotoController

    def prepare_default
      super
      @package = "reward"
      @resource = "reward"
      authenticate_backend
      @restful_model_class = Photo
      @restful_constants[:related_type] = "Reward"
      @restful_filters[:related_type] = "Reward"
    end
    
    def parse_create_and_update(data)
      if data.has_key?("related")
        data["related"] = convert_to_id data["related"], Reward
      end
      data
    end

  end
end