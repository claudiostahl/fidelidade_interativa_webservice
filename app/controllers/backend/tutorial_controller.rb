module Backend
  class TutorialController < CrudController

    def prepare_default
      @package = "default"
      @resource = "tutorial"
      authenticate_backend
      @restful_model_class = Tutorial
      @restful_attributes = [:id, :company, :name, :image, :order, :enabled, :created_at, :updated_at]
      @restful_permits = [:name, :image, :order, :enabled]
    end

  end
end