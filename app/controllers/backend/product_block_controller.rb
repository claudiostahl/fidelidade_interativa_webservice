module Backend
  class ProductBlockController < BlockController

    def prepare_default
      super
      @package = "product"
      @resource = "product"
      authenticate_backend
      @restful_constants[:group] = Block::GROUP_PRODUCT
      @restful_filters[:group] = Block::GROUP_PRODUCT
    end
    
    def parse_create_and_update(data)
      super
      if data.has_key?("related")
        data["related"] = convert_to_id data["related"], Product
      end
      data
    end

  end
end