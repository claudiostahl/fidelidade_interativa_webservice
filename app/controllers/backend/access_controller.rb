module Backend
  class AccessController < CrudController

    def prepare_default
      @package = "default"
      @resource = "access"
      authenticate_backend
      @restful_model_class = Access
      @restful_attributes = [:id, :company, :user, :related_id, :related_type, :created_at, :updated_at]
    end

  end
end