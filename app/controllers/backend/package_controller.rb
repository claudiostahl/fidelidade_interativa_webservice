module Backend
  class PackageController < CrudController

    def prepare_default
      @package = "default"
      @resource = "default"
      authenticate_backend
      @restful_model_class = Package
      @restful_attributes = [:code, :name]
      @restful_filters = {}
      @restful_constants = {}
    end

  end
end