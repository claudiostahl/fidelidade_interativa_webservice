module Backend
  class RewardBlockController < BlockController

    def prepare_default
      super
      @package = "reward"
      @resource = "reward"
      authenticate_backend
      @restful_constants[:group] = Block::GROUP_REWARD
      @restful_filters[:group] = Block::GROUP_REWARD
    end
    
    def parse_create_and_update(data)
      super
      if data.has_key?("related")
        data["related"] = convert_to_id data["related"], Reward
      end
      data
    end

  end
end