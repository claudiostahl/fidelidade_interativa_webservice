module Backend
  class UserGroupController < CrudController

    def prepare_default
      @package = "default"
      @resource = "user_group"
      authenticate_backend
      @restful_model_class = UserGroup
      @restful_attributes = [:id, :company, :name, :created_at, :updated_at]
      @restful_permits = [:name]
    end

  end
end