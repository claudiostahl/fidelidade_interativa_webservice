module Backend
  class PingController < ApplicationController

    def index
        render_json({:env => Rails.env, result: Time.now})
    end
    
    def post
        render_json({:env => Rails.env, result: Time.now, params: params.to_s})
    end

  end
end