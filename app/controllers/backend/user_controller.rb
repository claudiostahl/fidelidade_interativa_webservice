module Backend
  class UserController < CrudController

    def prepare_default
      @package = "default"
      @resource = "user"
      authenticate_backend
      @restful_model_class = User
      @restful_attributes = [:id, :company, :token, :name, :email, :language, :timezone, :facebook, :recover_token, :credit, :can_notification, :last_access_at, :last_buy_at, :enabled, :created_at, :updated_at]
      @restful_permits = [:name, :email, :language, :timezone, :password, :facebook, :enabled]
    end

  end
end