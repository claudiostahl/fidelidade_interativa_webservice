module Backend
  class BlockController < CrudController

    def prepare_default
      @restful_model_class = Block
      @restful_attributes = [:id, :company, :parent, :related_id, :related_type, :name, :layout, :order, :created_at, :updated_at]
      @restful_permits = [:parent, :related, :name, :layout, :order]
    end
    
    def parse_create_and_update(data)
      if data.has_key?("parent")
        data["parent"] = convert_to_id data["parent"], Block
      end
      data
    end

  end
end