module Backend
  class TranslationController < CrudController

    def prepare_default
      @related = params.require(:related).to_s
      @related_id = params.require(:related_id).to_s
      case @related
        when "banner"
          @related_type = Banner
          @package = "banner"
          @resource = "banner"
        when "product_block"
          @related_type = Block
          @package = "product"
          @resource = "product"
        when "reward_block"
          @related_type = Block
          @package = "reward"
          @resource = "reward"
        when "post"
          @related_type = Post
          @package = "post"
          @resource = "post"
        when "product"
          @related_type = Product
          @package = "product"
          @resource = "product"
        when "reward"
          @related_type = Reward
          @package = "reward"
          @resource = "reward"
        when "tutorial"
          @related_type = Tutorial
          @package = "default"
          @resource = "tutorial"
        else
          raise NotFoundException
      end
      authenticate_backend
      @restful_model_class = Translation
      @restful_attributes = [:id, :company, :language, :code, :value, :image, :created_at, :updated_at]
      @restful_permits = [:language, :related, :related_type, :code, :value, :image]
      @restful_filters[:related] = @related_type.where(id: @related_id).first
      @restful_filters[:related_type] = @related_type.to_s
    end
    
    def update
      result = process_data(:prepare_default) do |data|
        apply_config data
      end
      render_json result
    end
    
    def apply_config(values)
      related_id = @related_id
      related = @related_type.where(id: related_id.to_s).first
      if !related
        raise BadRequestException, "Invalid related"
      end
      related_type = @related_type.to_s
      if (@related == "product_block" && @related == "reward_block")
        block = convert_to_id related_id, Block
        if (@related == "product_block" && block.group != Block::GROUP_PRODUCT)
          raise BadRequestException, "Invalid translation"
        end
        if (@related == "reward_block" && block.group != Block::GROUP_REWARD)
          raise BadRequestException, "Invalid translation"
        end
      end
      language = convert_to values["language"], :abbreviation, Language
      code = values["code"]
      entity = Translation.where(related: related, related_type: related_type, code: code, company: @company, language: language).first
      data = {company: @company, language: language, related: related, related_type: related_type, code: code}
      if values.has_key? :value
        data["value"] = values["value"]
      end
      if values.has_key? :image
        data["image"] = values["image"]
      end
      
      if entity
        update_default @restful_model_class, entity.id, data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
      else
        create_default @restful_model_class, data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
      end
      
    end

  end
end