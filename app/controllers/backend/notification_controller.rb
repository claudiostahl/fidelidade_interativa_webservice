module Backend
  class NotificationController < CrudController

    def prepare_default
      @package = "defualt"
      @resource = "notification"
      authenticate_backend
      @restful_model_class = Notification
      @restful_attributes = [:id, :company, :user, :name, :description, :read, :arquived, :created_at, :updated_at]
    end

  end
end