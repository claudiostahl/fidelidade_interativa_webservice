module Backend
  class RewardRuleController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "reward"
      authenticate_backend
      @restful_model_class = RewardRule
      @restful_attributes = [:id, :company, :reward, :week_day, :hour_start, :hour_finish, :created_at, :updated_at]
      @restful_permits = [:reward, :week_day, :hour_start, :hour_finish]
    end
    
    def parse_create_and_update(data)
      if data.has_key?("reward")
        data["reward"] = convert_to_id data["reward"], Reward
      end
      data
    end

  end
end