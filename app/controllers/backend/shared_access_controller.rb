module Backend
  class SharedAccessController < CrudController

    def prepare_default
      @package = "default"
      @resource = "shared"
      authenticate_backend
      @restful_model_class = SharedAccess
      @restful_attributes = [:id, :company, :shared, :device, :ip, :created_at, :updated_at]
    end

  end
end