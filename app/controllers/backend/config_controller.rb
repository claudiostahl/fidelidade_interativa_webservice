module Backend
  class ConfigController < CrudController

    def prepare_default
      @package = "default"
      @resource = "config"
      authenticate_backend
      @restful_model_class = Config
      @restful_attributes = [:id, :company, :code, :value]
      @restful_permits = [:code, :value]
    end
    
    def update
      result = process_data(:prepare_default) do |data|
        apply_config data
      end
      render_json result
    end
    
    def apply_config(values)
      code = values["code"]
      value = values["value"]
      entity = Config.where(code: code, company: @company).first
      if entity
        data = entity.attributes
        data[:value] = value
        update_default @restful_model_class, entity.id, data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
      else
        data = {company: @company, code: values["code"], value: value}
        create_default @restful_model_class, data, @restful_attributes, @restful_permits, @restful_constants, @restful_filters
      end
    end

  end
end