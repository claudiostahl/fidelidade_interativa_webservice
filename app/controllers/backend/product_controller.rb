module Backend
  class ProductController < CrudController

    def prepare_default
      @package = "product"
      @resource = "product"
      authenticate_backend
      @restful_model_class = Product
      @restful_attributes = [:id, :company, :related_products, :name, :slug, :short_description, :description, :price, :special_price, :first_vote, :rate, :enabled, :created_at, :updated_at]
      @restful_permits = [:related_products, :name, :slug, :short_description, :description, :price, :special_price, :first_vote, :enabled]
    end
    
    def parse_create_and_update(data)
      if data.has_key?("related_products")
        data["related_products"] = convert_array_to_ids data["related_products"], Product
      end
      data
    end

  end
end