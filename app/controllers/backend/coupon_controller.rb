module Backend
  class CouponController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "coupon"
      authenticate_backend
      @restful_model_class = Coupon
      @restful_attributes = [:id, :company, :coupon_group, :token, :used, :created_at, :updated_at]
      @restful_permits = [:coupon_group]
    end
    
    def parse_create_and_update(data)
      if data.has_key?("coupon_group")
        data["coupon_group"] = convert_to_id data["coupon_group"], CouponGroup
      end
      data
    end

  end
end