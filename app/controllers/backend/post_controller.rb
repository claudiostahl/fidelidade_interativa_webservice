module Backend
  class PostController < CrudController

    def prepare_default
      @package = "post"
      @resource = "post"
      authenticate_backend
      @restful_model_class = Post
      @restful_attributes = [:id, :company, :title, :slug, :description, :content, :publication_date, :enabled, :created_at, :updated_at]
      @restful_permits = [:title, :slug, :description, :content, :publication_date, :enabled]
    end

  end
end