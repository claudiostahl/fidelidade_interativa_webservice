module Backend
  class ProductPhotoController < PhotoController

    def prepare_default
      super
      @package = "product"
      @resource = "product"
      authenticate_backend
      @restful_constants[:related_type] = "Product"
      @restful_filters[:related_type] = "Product"
    end
    
    def parse_create_and_update(data)
      if data.has_key?("related")
        data["related"] = convert_to_id data["related"], Product
      end
      data
    end

  end
end