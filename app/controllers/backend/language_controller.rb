module Backend
  class LanguageController < CrudController

    def prepare_default
      @restful_model_class = Language
      @restful_attributes = [:id, :name, :abbreviation, :created_at, :updated_at]
      @restful_permits = [:name, :abbreviation]
      @restful_constants = {}
      @restful_filters = {}
    end
    
    def prepare_create_and_update
      authenticate_backend
      check_root
    end

  end
end