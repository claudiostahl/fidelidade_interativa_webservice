module Backend
  class CouponReadController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "coupon_read"
      authenticate_backend
      @restful_model_class = CouponRead
      @restful_attributes = [:id, :company, :coupon_group, :coupon, :user, :value, :created_at, :updated_at]
    end

  end
end