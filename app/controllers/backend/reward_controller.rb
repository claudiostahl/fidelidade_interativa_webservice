module Backend
  class RewardController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "reward"
      authenticate_backend
      @restful_model_class = Reward
      @restful_attributes = [
        :id, 
        :company, 
        :user_group,
        :name, 
        :slug, 
        :short_description, 
        :description, 
        :rule_description, 
        :value, 
        :price, 
        :first_vote, 
        :rate,
        :publication_date, 
        :limit, 
        :limit_user, 
        :date_start, 
        :date_finish,
        :enabled, 
        :created_at, 
        :updated_at
      ]
      @restful_permits = [
        :id, 
        :user_group,
        :name, 
        :slug, 
        :short_description, 
        :description, 
        :rule_description, 
        :value, 
        :price, 
        :first_vote, 
        :publication_date, 
        :limit, 
        :limit_user,
        :date_start, 
        :date_finish,
        :enabled
      ]
    end
    
    def parse_create_and_update(data)
      if data.has_key?("user_group")
        data["user_group"] = convert_to_id data["user_group"], UserGroup
      end
      data
    end

  end
end