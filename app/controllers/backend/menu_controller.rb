module Backend
  class MenuController < CrudController

    def prepare_default
      @package = "menu"
      @resource = "menu"
      authenticate_backend
      @restful_model_class = Menu
      @restful_attributes = [:id, :company, :image, :title, :url, :content, :order, :enabled, :created_at, :updated_at]
      @restful_permits = [:image, :title, :url, :content, :order, :enabled]
    end

  end
end