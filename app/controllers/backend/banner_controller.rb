module Backend
  class BannerController < CrudController

    def prepare_default
      @package = "banner"
      @resource = "banner"
      authenticate_backend
      @restful_model_class = Banner
      @restful_attributes = [:id, :company, :administrator, :link, :name, :image, :enabled, :order, :created_at, :updated_at]
      @restful_permits = [:link, :name, :image, :enabled, :order]
      @restful_constants[:administrator] = @administrator
    end

  end
end