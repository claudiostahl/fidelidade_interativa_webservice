module Backend
  class PremiumTypeController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "premium_type"
      authenticate_backend
      @restful_model_class = PremiumType
      @restful_attributes = [:id, :company, :token, :name, :description, :notification_title, :notification_description, :value, :limit, :limit_user, :validity, :enabled, :created_at, :updated_at]
      @restful_permits = [:name, :description, :notification_title, :notification_description, :value, :limit, :limit_user, :validity, :enabled]
    end

  end
end