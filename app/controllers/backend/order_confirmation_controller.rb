module Backend
  class OrderConfirmationController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "order"
      authenticate_backend
      @restful_model_class = OrderConfirmation
      @restful_attributes = [:id, :company, :order, :administrator, :comments, :created_at, :updated_at]
      @restful_permits = [:order, :comments]
      @restful_constants[:administrator] = @administrator
    end
    
    def prepare_update
      @restful_permits = [:comments]
    end
    
    def parse_create(data)
      if data.has_key? "order"
        data["order"] = convert_to_id data["order"], Order
      end
      data
    end

  end
end