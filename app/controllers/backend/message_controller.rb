module Backend
  class MessageController < CrudController

    def prepare_default
      @package = "message"
      @resource = "message"
      authenticate_backend
      @restful_model_class = Message
      @restful_attributes = [:id, :company, :administrator, :message_dispatches, :link, :name, :description, :image, :publication_date, :created_at, :updated_at]
      @restful_permits = [:link, :name, :description, :image, :publication_date]
      @restful_constants[:administrator] = @administrator
    end
    
    def parse_create_and_update_constants(model, constants, data)
      if data.has_key?("users")
        users = data["users"]
        messages = []
        if (users.kind_of?(Array) && users.count)
          for string_id in users
            user = convert_to_id string_id, User
            messages << MessageDispatch.build_from_user(@company, user)
          end
        end
        constants[:message_dispatches] = messages
      end
      constants
    end

  end
end