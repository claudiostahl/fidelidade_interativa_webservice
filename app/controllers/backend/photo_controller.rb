module Backend
  class PhotoController < CrudController

    def prepare_default
      @restful_model_class = Photo
      @restful_attributes = [:id, :company, :related_id, :related_type, :code, :image, :order, :enabled, :created_at, :updated_at]
      @restful_permits = [:related, :code, :image, :order, :enabled]
    end

  end
end