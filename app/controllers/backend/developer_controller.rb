module Backend
  class DeveloperController < CrudController

    def prepare_default
      @package = "developer"
      @resource = "developer"
      authenticate_backend
      @restful_model_class = Developer
      @restful_attributes = [:id, :company, :resources, :name, :email, :token, :enabled, :created_at, :updated_at]
      @restful_permits = [:resources, :name, :email, :enabled]
    end
    
    def refresh_token
      result = process_one(:prepare_default) do
        id = params.require(:id)
        developer = Developer.where(id: id).first
        if !developer
          raise NotFoundException
        end
        developer.generate_token
        developer.save!
        developer.builder_json_by_array @restful_attributes
      end
      render_json result
    end
    
    def parse_create_and_update(data)
      if data.has_key?("resources")
        data["resources"] = convert_array_to data["resources"], :code, Resource
      end
      data
    end

  end
end