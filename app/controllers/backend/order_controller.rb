module Backend
  class OrderController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "order"
      authenticate_backend
      @restful_model_class = Order
      @restful_attributes = [:id, :company, :user, :administrator, :reward, :token, :comments, :reward_name, :reward_slug, :reward_short_description, :reward_description, :reward_rule_description, :value, :price, :invoiced_value, :status, :created_at, :updated_at]
      @restful_permits = [:user, :reward, :comments]
      @restful_constants[:administrator] = @administrator
    end
    
    def prepare_update
      @restful_permits = [:comments]
    end
    
    def parse_create(data)
      if data.has_key?("user")
        data["user"] = convert_to_id data["user"], User
      end
      if data.has_key?("reward")
        data["reward"] = convert_to_id data["reward"], Reward
      end
      data
    end

  end
end