module Backend
  class RecoverController < ApplicationController

    def create
      result = process_one do
        login = params.require :login
        administrator = Administrator.where(email: login).first
        if !administrator
          raise BadRequestException, "Invalid email"
        end
        administrator.create_recover
        administrator.save!
        return_ok
      end
      render_json result
    end
    
    def update
      result = process_one do
        token = params.require :token
        password = params.require :password
        administrator = Administrator.where(recover_token: token).first
        if !administrator
          raise BadRequestException, "Invalid token"
        end
        administrator.password = password
        administrator.save!
        return_ok
      end
      render_json result
    end

  end
end