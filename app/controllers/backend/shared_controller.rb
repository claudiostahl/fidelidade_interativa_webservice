module Backend
  class SharedController < CrudController

    def prepare_default
      @package = "default"
      @resource = "shared"
      authenticate_backend
      @restful_model_class = Shared
      @restful_attributes = [:id, :company, :user, :related_id, :related_type, :token, :created_at, :updated_at]
      @restful_permits = [:related_id, :related_type]
    end

  end
end