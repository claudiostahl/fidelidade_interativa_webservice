module Backend
  class CashflowController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "cashflow"
      authenticate_backend
      @restful_model_class = Cashflow
      @restful_attributes = [:id, :company, :user, :related_id, :related_type, :type_flow, :value, :created_at, :updated_at]
    end

  end
end