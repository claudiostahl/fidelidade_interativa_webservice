module Backend
  class ConnectionController < ApplicationController

    def login
      result = process_one do
        if (!params.has_key?("company") || !params.has_key?("login") || !params.has_key?("password"))
          raise BadRequestException, "Invalid data"
        end
        field_company = params["company"]
        field_login = params["login"]
        field_password = params["password"]
        company = Company.where(code: field_company).first
        administrator = Administrator.where(email: field_login).first
        if (!company || !administrator || !administrator.is_password?(field_password) || (!administrator.is_root? && !administrator.companies.include?(company)))
          raise BadRequestException, "Invalid credentials"
        end
        if (!administrator.enabled?)
          raise UnauthorizedException, "Disabled user"
        end
        if !company.enabled_backend?
          raise UnauthorizedException, "Backend disabled"
        end
        connection = Connection.build_default(company)
        administrator.connections.push(connection)
        administrator.save
        output = connection.builder_json_by_array([:device, :token, :created_at])
        output[:administrator] = administrator.builder_json_by_array([:id, :companies, :resources, :language, :name, :email, :is_root, :enabled, :timezone])
        output[:company] = company.builder_json_by_array([:id, :packages, :languages, :code, :name, :token, :enabled_backend, :enabled_frontend, :enabled_api])
        return_created output
      end
      render_json result
    end

    def logout
      result = process_one do
        authenticate_backend
        @connection.destroy!
        return_ok
      end
      render_json result
    end

  end
end