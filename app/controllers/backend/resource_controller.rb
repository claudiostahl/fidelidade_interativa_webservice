module Backend
  class ResourceController < CrudController

    def prepare_default
      @package = "default"
      @resource = "default"
      authenticate_backend
      @restful_model_class = Resource
      @restful_attributes = [:code, :name]
      @restful_filters = {}
      @restful_constants = {}
    end

  end
end