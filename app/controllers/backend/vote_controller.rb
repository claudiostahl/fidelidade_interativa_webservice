module Backend
  class VoteController < CrudController

    def prepare_default
      @package = "default"
      @resource = "vote"
      authenticate_backend
      @restful_model_class = Vote
      @restful_attributes = [:id, :company, :related_id, :related_type, :user, :vote, :created_at, :updated_at]
    end

  end
end