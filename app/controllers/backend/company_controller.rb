module Backend
  class CompanyController < CrudController

    def prepare_default
      authenticate_backend
      check_root
      @restful_model_class = Company
      @restful_attributes = [:id, :packages, :languages, :code, :name, :token, :enabled_backend, :enabled_frontend, :enabled_api, :created_at, :updated_at]
      @restful_permits = [:packages, :languages, :code, :name, :enabled_backend, :enabled_frontend, :enabled_api]
      @restful_constants = {}
      @restful_filters = {}
    end
    
    def prepare_create_and_update
      check_password
    end
    
    def parse_create_and_update(data)
      if data.has_key?("packages")
        data["packages"] = convert_array_to data["packages"], :code, Package
      end
      if data.has_key?("languages")
        data["languages"] = convert_array_to data["languages"], :abbreviation, Language
      end
      data
    end

  end
end