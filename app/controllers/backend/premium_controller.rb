module Backend
  class PremiumController < CrudController

    def prepare_default
      @package = "reward"
      @resource = "premium"
      authenticate_backend
      @restful_model_class = Premium
      @restful_attributes = [:id, :company, :user, :premium_type, :developer, :campaign, :value, :created_at, :updated_at]
    end

  end
end