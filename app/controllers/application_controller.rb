class ApplicationController < ActionController::Base

  include ApplicationControllerDefault
  include ApplicationControllerProcess
  include ApplicationControllerUtil
  include ApplicationControllerReturn

  def initialize
    @device = nil
    @email = nil
    @token = nil
    @password = nil
    @connection = nil
    @company = nil
    @package = nil
    @resource = nil
    @administrator = nil
    @user = nil
    @data = {}
    @restful_model_class = nil
    @restful_attributes = []
    @restful_permits = []
    @restful_where = {}
    @restful_order = {}
    @restful_limit = 1000
    @restful_offset = 0
    @restful_constants = {}
    @restful_filters = {}
  end
  
  before_action :parse_request

  def parse_request
    if params.has_key?(:device)
      @device = params[:device]
    end
    if params.has_key?(:email)
      @email = params[:email]
    end
    if params.has_key?(:token)
      @token = params[:token]
    end
    if params.has_key?(:password)
      @password = params[:password]
    end
  end
  
  def authenticate
    if !@device || !@token
      raise UnauthorizedException, "Invalid Api-Device or Api-Token"
    else 
      @connection = Connection.where(device: @device, token: @token).first
      if !@connection
        raise UnauthorizedException, "Unauthorized access"
      end
      @company = @connection.company
    end
  end
  
  def authenticate_backend
    authenticate
    @administrator = @connection.related
    if !@administrator.kind_of?(Administrator)
      raise UnauthorizedException, "Invalid token"
    end
    if @package
      model_package = Package.where(code: @package).first!
      if !@company.packages.include?(model_package)
        raise UnauthorizedException, "Unauthorized package access"
      end
    end
    if @resource
      model_resource = Resource.where(code: @resource).first!
      if !@administrator.resources.include?(model_resource) && !@administrator.is_root?
        raise UnauthorizedException, "Unauthorized resource access"
      end
    end
    if !@company.enabled_backend?
      raise UnauthorizedException, "Backend disabled"
    end
    @restful_constants = {company: @company}
    @restful_filters = {company: @company}
    Log.build_and_save(@company,  @administrator, params.to_s)
  end
  
  def authenticate_frontend
    authenticate
    @user = @connection.related
    if !@user.kind_of?(User)
      raise UnauthorizedException, "Invalid token"
    end
    if !@company.enabled_frontend?
      raise UnauthorizedException, "Frontend disabled"
    end
    @user.last_access_at = DateTime.now 
    @user.save!
    @restful_constants = {company: @company, user: @user}
    @restful_filters = {company: @company, user: @user}
    Log.build_and_save(@company,  @user, params.to_s)
  end
  
  def check_password
    if !@administrator.is_password?(@password)
      raise UnauthorizedException, "Password required"
    end
  end
  
  def check_root
    if !@administrator.is_root?
      raise UnauthorizedException, "Root access required"
    end
  end
  
  def authenticate_api
    if !@email || !@token
      raise UnauthorizedException, "Invalid Api-Email or Api-Token"
    end 
    @developer = Developer.where(email: @email, token: @token).first
    if !@developer
      raise UnauthorizedException, "Unauthorized access"
    end
    @company = @developer.company
    if @package
      model_package = Package.where(code: @package).first!
      if !@company.packages.include?(model_package)
        raise UnauthorizedException, "Unauthorized package access"
      end
    end
    if @resource
      model_resource = Resource.where(code: @resource).first!
      if !@developer.resources.include?(model_resource)
        raise UnauthorizedException, "Unauthorized resource access"
      end
    end
    if !@company.enabled_api?
      raise UnauthorizedException, "Api disabled"
    end
    @restful_constants = {company: @company}
    @restful_filters = {company: @company}
    Log.build_and_save(@company,  @developer, params.to_s)
  end

end