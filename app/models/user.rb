class User < ActiveRecord::Base
  include BCrypt
  
  self.table_name = "user"
    
  belongs_to :company
  has_and_belongs_to_many :user_groups, dependent: :destroy
  has_many :logs, as: :related, dependent: :nullify
  
  validates :company, presence: true
  validates :token, presence: true, uniqueness: true, length: { maximum: 200 }
  validates :name, presence: true, length: { maximum: 200 }
  validates :email, presence: true, email: true, uniqueness: {scope: :company}, length: { maximum: 200 }
  validates :language, length: { maximum: 200 }
  validates :password_hash, length: { maximum: 200 }
  validates :facebook, length: { maximum: 200 }
  validates :recover_token, length: { maximum: 200 }
  validates :credit, numericality: true
  validates :can_notification, boolean: true
  validates :timezone, numericality: { only_integer: true, allow_blank: true }
  validates :enabled, boolean: true
  
  validates_datetime :last_access_at, allow_nil: true
  validates_datetime :last_buy_at, allow_nil: true
  
  after_initialize :set_default, :if => :new_record?
  
  def set_default
    self.token = Digest::SHA1.hexdigest Time.now.to_f.to_s
    self.last_access_at = DateTime.new
    self.last_buy_at = DateTime.new
  end
  
  def update_last_buy_at_and_save
    self.last_buy_at = DateTime.now 
    self.save!
    self
  end
  
  def password
    @password ||= Password.new(password_hash)
  end

  def password=(new_password)
    @password = Password.create(new_password)
    self.password_hash = @password
  end
  
  def is_password?(password)
    self.password == password && password.present?
  end
  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.is_password?(password)
      user
    else
      nil
    end
  end
  
end
