class Order < ActiveRecord::Base
    self.table_name = "order"
    
    STATUS_PENDING = 1
    STATUS_CONFIRMED = 2
    STATUS_CANCELED = 3
    
    belongs_to :company
    belongs_to :user
    belongs_to :administrator
    belongs_to :reward
    has_many :order_confirmations, dependent: :restrict_with_exception
    has_many :order_refunds, dependent: :restrict_with_exception
    has_many :order_canceleds, dependent: :restrict_with_exception
    has_many :cashflows, as: :related, dependent: :restrict_with_exception
    
    validates :company, presence: true
    validates :user, presence: true
    validates :reward, presence: true
    validates :token, presence: true, uniqueness: {scope: :company}, length: { maximum: 80 }
    validates :comments, length: { maximum: 65536 }
    validates :reward_name, presence: true, length: { maximum: 200 }
    validates :reward_slug, presence: true, length: { maximum: 200 }
    validates :reward_short_description, presence: true, length: { maximum: 200 }
    validates :reward_description, presence: true, length: { maximum: 65536 }
    validates :reward_rule_description, presence: true, length: { maximum: 65536 }
    validates :value, presence: true, numericality: true
    validates :price, presence: true, numericality: true
    validates :invoiced_value, presence: true, numericality: true
    validates :status, presence: true, inclusion: {:in => 1..3}
    
    after_initialize :set_default, :if => :new_record?
    before_save :before_save, :if => :new_record?
    after_save :after_save, :if => :new_record?
  
    def set_default
        self.token = Digest::SHA1.hexdigest Time.now.to_f.to_s
        self.invoiced_value = 0
        self.status = STATUS_PENDING
    end
    
    def before_save
        if self.value > self.user.credit
            raise BadRequestException, "Insufficient credit"
        end
    end
    
    def after_save
        Cashflow.build_and_save(self.company, self.user, self, Cashflow::TYPE_ORDER_NEW, self.value)
        
        if self.administrator = nil
            self.user.update_last_buy_at_and_save
        end
    end
    
    def reward=(reward)
        super        
        self.reward_name = reward.name
        self.reward_slug = reward.slug
        self.reward_short_description = reward.short_description
        self.reward_description = reward.description
        self.reward_rule_description = reward.rule_description
        self.value = reward.value
        self.price = reward.price
    end
    
    def refund_and_save(value)
       self.invoiced_value -= value
       self.save! 
       self
    end
    
    def cancel_and_save
        self.invoiced_value = 0
        self.status = Order::STATUS_CANCELED
        self.save! 
        self
    end
    
    def confirmation_and_save
        self.invoiced_value = self.value
        self.status = Order::STATUS_CONFIRMED
        self.save! 
        self
    end
    
    def validate_reward!
        # test reward enabled
        if !self.reward.enabled?
            raise BadRequestException, "Reward disabled" 
        end
        # test limit
        others = Order.where(reward: self.reward).count
        if others > self.reward.limit
           raise BadRequestException, "Exceeded the limit" 
        end
        # test limit by user
        othersWithUser = Order.where(reward: self.reward).where(user: self.user).count
        if othersWithUser > self.reward.limit_user
           raise BadRequestException, "Exceeded the limit by user" 
        end
        # test date start
        if self.date_start > Time.now
            raise BadRequestException, "Award has not yet begun" 
        end
        # test date finish
        if self.date_finish < Time.now
            raise BadRequestException, "Award already ended" 
        end
        # test user group
        if self.user_group != nil && !self.user.user_group.include?(self.user_group)
            raise BadRequestException, "Invalid user group"
        end
        # test rules
        rules = RewardRule.where(reward: self.reward)
        rules_valid = false
        for rule in rules
            rule_valid = true
            if rule.week_day != nil && rule.week_day.wday != Time.now.wday
                rule_valid = false
            end
            if rule.hour_start != nil && rule.hour_start.convert_to_second > Time.now.convert_to_second
                rule_valid = false
            end
            if rule.hour_finish != nil && rule.hour_finish.convert_to_second < Time.now.convert_to_second
                rule_valid = false
            end
            if rule_valid
               rules_valid = true
            end
        end
        if !rules_valid 
            raise BadRequestException, "Did not pass the rules"
        end
    end
end