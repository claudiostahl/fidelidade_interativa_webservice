class Administrator < ActiveRecord::Base
  include BCrypt
  
  self.table_name = "administrator"
  
  belongs_to :language
  
  has_and_belongs_to_many :companies, dependent: :destroy
  has_and_belongs_to_many :resources, dependent: :destroy
  has_many :connections, as: :related, dependent: :destroy
  has_many :logs, as: :related, dependent: :nullify

  validates :name, presence: true, length: { maximum: 200 }
  validates :email, presence: true, email: true, uniqueness: true, length: { maximum: 200 }
  validates :password_hash, length: { maximum: 200 }
  validates :recover_token, length: { maximum: 200 }
  validates :timezone, numericality: { only_integer: true, allow_blank: true }
  validates :enabled, boolean: true
  validates :is_root, boolean: true
  
  def password
    @password ||= Password.new(password_hash)
  end

  def password=(new_password)
    if new_password.blank?
      raise InvalidException, "Password can't be blank"
    end
    @password = Password.create(new_password)
    self.password_hash = @password
  end
  
  def is_password?(password)
    self.password == password && password.present?
  end
  
  def create_recover
    self.recover_token = Digest::SHA1.hexdigest Time.now.to_f.to_s
  end
  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.is_password?(password)
      user
    else
      nil
    end
  end

end