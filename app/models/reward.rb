class Reward < ActiveRecord::Base
    self.table_name = "reward"
    
    belongs_to :company
    belongs_to :user_group
    has_many :blocks, as: :related, dependent: :destroy
    has_many :photos, as: :related, dependent: :destroy
    has_many :votes, as: :related, dependent: :destroy
    has_many :accesses, as: :related, dependent: :destroy
    has_many :shareds, as: :related, dependent: :nullify
    has_many :translations, as: :related, dependent: :destroy
    
    validates :company, presence: true
    validates :name, presence: true, length: { maximum: 200 }
    validates :slug, presence: true, uniqueness: {scope: :company}, length: { maximum: 200 }
    validates :short_description, presence: true, length: { maximum: 200 }
    validates :description, presence: true, length: { maximum: 65536 }
    validates :rule_description, presence: true, length: { maximum: 65536 }
    validates :value, presence: true, numericality: true
    validates :price, presence: true, numericality: true
    validates :first_vote, presence: true, numericality: true
    validates :rate, presence: true, numericality: true
    validates :publication_date, presence: true
    validates :limit, presence: true, numericality: { only_integer: true }
    validates :limit_user, presence: true, numericality: { only_integer: true }
    validates :date_start, presence: true
    validates :date_finish, presence: true
    validates :enabled, boolean: true
    
    validates_datetime :publication_date
    validates_datetime :date_start
    validates_datetime :date_finish
    
    def first_vote=(first_vote)
        super
        if first_vote.is_number?
            first_vote = first_vote.to_f if first_vote.is_a? String
            first_vote = 0 if first_vote <= 0
            first_vote = 10 if first_vote >= 10 
        end
        total = first_vote
        count = 1
        votes = Vote.where(related: self, related_type: "Reward")
        for vote in votes
            total+=vote.vote
            count+=1
        end
        self.rate = total/count
    end
    
end