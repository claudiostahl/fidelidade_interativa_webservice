class Menu < ActiveRecord::Base
    self.table_name = "menu"
    
    belongs_to :company
    
    validates :company, presence: true
    validates :title, presence: true, length: { maximum: 200 }
    validates :url, length: { maximum: 200 }
    validates :order, numericality: { only_integer: true, allow_blank: true }
    validates :content, length: { maximum: 65536 }
    validates :enabled, boolean: true
    
    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment_content_type :image, :content_type => %w(image/jpeg image/jpg image/png)
end
