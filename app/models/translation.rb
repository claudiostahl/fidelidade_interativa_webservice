class Translation < ActiveRecord::Base
    self.table_name = "translation"
    
    belongs_to :company
    belongs_to :language
    belongs_to :related, polymorphic: true
    
    validates :company, presence: true
    validates :language, presence: true
    validates :related, presence: true
    validates :code, presence: true, length: { maximum: 80 }
    validates :value_string, length: { maximum: 200 }
    validates :value_text, length: { maximum: 65536 }
    validates :value_number, numericality: { allow_blank: true }
    validates :value_boolean, boolean: { allow_blank: true }
    validates_datetime :value_datetime, allow_nil: true
    
    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment_content_type :image, :content_type => %w(image/jpeg image/jpg image/png)
    
    def self.value_by_code(company, code, language, related)
        begin
            translation = Translation.where(code: code, company: company, language: language, related: related).first!
            translation.value
        rescue ActiveRecord::RecordNotFound
            nil
        end
    end
    
    def value=(value)
        populate_custom_value value
    end
    
    def value
        get_custom_value
    end
    
    def as_json(options = nil)
        output = super
        output["value"] = value
        output
    end
    
    def image_names
       [:image] 
    end
    
    def image_styles
       [:medium, :thumb] 
    end
end
