class Shared < ActiveRecord::Base
    self.table_name = "shared"
    
    belongs_to :company
    belongs_to :user
    belongs_to :related, polymorphic: true
    
    validates :company, presence: true
    validates :user, presence: true
    validates :related, presence: true
    validates :token, presence: true, length: { maximum: 80 }
    
    after_initialize :set_default, :if => :new_record?
    before_save :before_save
  
    def set_default
        self.token = Digest::SHA1.hexdigest Time.now.to_f.to_s
    end
    
    def before_save
        if self.related.company.id != self.company.id
            raise BadRequestException, "Related invalid" 
        end
    end
end
