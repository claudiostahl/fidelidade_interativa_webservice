class RewardRule < ActiveRecord::Base
    self.table_name = "reward_rule"
    
    belongs_to :company
    belongs_to :reward
    
    validates :company, presence: true
    validates :reward, presence: true
    validates :week_day, numericality: { only_integer: true, allow_blank: true }
    
    validates_time :hour_start, allow_nil: true
    validates_time :hour_finish, allow_nil: true
end
