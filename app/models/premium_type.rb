class PremiumType < ActiveRecord::Base
    self.table_name = "premium_type"
    
    belongs_to :company
    
    validates :company, presence: true
    validates :token, presence: true, uniqueness: true, length: { maximum: 200 }
    validates :name, presence: true, length: { maximum: 200 }
    validates :description, presence: true, length: { maximum: 65536 }
    validates :notification_title, presence: true, length: { maximum: 200 }
    validates :notification_description, presence: true, length: { maximum: 65536 }
    validates :value, presence: true, numericality: true
    validates :limit, numericality: { only_integer: true }
    validates :limit_user, numericality: { only_integer: true }
    validates :enabled, boolean: true
    validates_datetime :validity
    
    after_initialize :set_default, :if => :new_record?
  
    def set_default
        self.token = Digest::SHA1.hexdigest Time.now.to_f.to_s
    end
end
