class Banner < ActiveRecord::Base
    self.table_name = "banner"
    
    belongs_to :company
    belongs_to :administrator
    has_many :accesses, as: :related, dependent: :destroy
    has_many :translations, as: :related, dependent: :destroy
    
    validates :company, presence: true
    validates :administrator, presence: true  
    validates :link, length: { maximum: 200 }
    validates :name, presence: true, length: { maximum: 200 }
    validates :image, presence: true
    validates :enabled, boolean: true
    validates :order, numericality: { only_integer: true, allow_blank: true }
    
    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment_content_type :image, :content_type => %w(image/jpeg image/jpg image/png)
    
    def image_names
       [:image] 
    end
    
    def image_styles
       [:medium, :thumb] 
    end
end
