class Access < ActiveRecord::Base
    self.table_name = "access"
    
    belongs_to :company
    belongs_to :user
    belongs_to :related, polymorphic: true
    
    validates :company, presence: true
    validates :user, presence: true
    validates :related, presence: true
    
    before_save :before_save
    
    def before_save
        if self.related.company.id != self.company.id
            raise BadRequestException, "Related invalid" 
        end
    end
end