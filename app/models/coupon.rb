class Coupon < ActiveRecord::Base
    self.table_name = "coupon"
    
    belongs_to :company
    belongs_to :coupon_group
    
    validates :company, presence: true
    validates :coupon_group, presence: true
    validates :token, presence: true, length: { maximum: 80 }
    validates :used, presence: true, numericality: { only_integer: true }
    
    after_initialize :set_default, :if => :new_record?
  
    def set_default
        self.token = Digest::SHA1.hexdigest Time.now.to_f.to_s
    end
    
    def increment_and_save
      self.used+=1
      self.save! 
      self
    end
end
