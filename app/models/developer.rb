class Developer < ActiveRecord::Base
  
  self.table_name = "developer"
  
  belongs_to :company
  has_and_belongs_to_many :resources, dependent: :destroy
  has_many :logs, as: :related, dependent: :nullify

  validates :name, presence: true, length: { maximum: 200 }
  validates :email, presence: true, email: true, uniqueness: {scope: :company}, length: { maximum: 200 }
  validates :token, presence: true, uniqueness: true, length: { maximum: 200 }
  validates :enabled, boolean: true
  
  after_initialize :set_default, :if => :new_record?
  
  def set_default
    generate_token
  end
  
  def generate_token
    self.token = Digest::SHA1.hexdigest Time.now.to_f.to_s
  end

end