class Package < ActiveRecord::Base
    self.table_name = "package"
    
    has_and_belongs_to_many :companies, dependent: :destroy
    
    validates :code, presence: true, uniqueness: true, length: { maximum: 200 }
    validates :name, presence: true, uniqueness: true, length: { maximum: 200 }
    
    def related_attribute
       self.code 
    end
end
