class OrderCanceled < ActiveRecord::Base
    self.table_name = "order_canceled"
    
    belongs_to :company
    belongs_to :order
    belongs_to :administrator
    
    validates :company, presence: true
    validates :order, presence: true
    validates :administrator, presence: true
    validates :system, boolean: true
    validates :comments, length: { maximum: 200 }
    
    before_save :before_save, :if => :new_record?
    after_save :after_save, :if => :new_record?
    
    def before_save
        if self.order.status == Order::STATUS_CANCELED
            raise BadRequestException, "Order already canceled"
        end
    end
    
    def after_save
        cashflow_value = self.order.invoiced_value * -1
        
        self.order.cancel_and_save
       
        Cashflow.build_and_save(self.company, self.order.user, self.order, Cashflow::TYPE_ORDER_CANCELED, cashflow_value)
        
        Notification.build_and_save(self.company, self.order.user, "order_canceled", {order_token: self.order.token})
    end
end
