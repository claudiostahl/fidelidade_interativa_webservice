class Config < ActiveRecord::Base
    
    self.table_name = "config"
    
    belongs_to :company
    
    validates :company, presence: true
    validates :code, presence: true, uniqueness: {scope: :company}, length: { maximum: 200 }
    validates :value_string, length: { maximum: 200 }
    validates :value_text, length: { maximum: 65536 }
    validates :value_number, numericality: { allow_blank: true }
    validates :value_boolean, boolean: { allow_blank: true }
    validates_datetime :value_datetime, allow_nil: true
    
    def value=(value)
        populate_custom_value value
    end
    
    def value
        get_custom_value
    end
    
    def as_json(options = nil)
        output = super
        output["value"] = value
        output
    end
    
    def self.create_by_code(company, code, value)
        data = {company: self, code: code, value: value}
        Config.create(data).save!
    end
    
    def self.value_by_code(company, code)
        begin
            config = Config.where(company: company).where(code: code).first!
            config.value
        rescue ActiveRecord::RecordNotFound
            nil
        end
    end
    
end
