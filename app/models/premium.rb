class Premium < ActiveRecord::Base
    self.table_name = "premium"
    
    belongs_to :company
    belongs_to :user
    belongs_to :premium_type
    belongs_to :developer
    
    validates :company, presence: true
    validates :user, presence: true
    validates :premium_type, presence: true
    validates :developer, presence: true
    validates :campaign, presence: true, length: { maximum: 200 }
    validates :premium_type_name, presence: true, length: { maximum: 200 }
    validates :premium_type_description, presence: true, length: { maximum: 65536 }
    validates :premium_type_notification_title, presence: true, length: { maximum: 200 }
    validates :premium_type_notification_description, presence: true, length: { maximum: 65536 }
    validates :value, presence: true, numericality: true
    
    after_save :after_save, :if => :new_record?
    
    def after_save
      Cashflow.build_and_save(self.company, :self.user, self, Cashflow::TYPE_PREMIUM, self.value)
      
      Notification.build_premium_and_save(self)
    end
    
    def validate_limit!
      others = Premium.where(premium_type: self.premium_type).count
      if others > self.premium_type.limit
        raise BadRequestException, "Exceeded the limit" 
      end
      othersWithUser = Premium.where(premium_type: self.premium_type).where(user: self.user).count
        if othersWithUser > self.premium_type.limit_user
           raise BadRequestException, "Exceeded the limit by user" 
        end
    end
    
    def premium_type=(premium_type)
      super
      self.premium_type_name = premium_type.name
      self.premium_type_description = premium_type.description
      self.premium_type_notification_title = premium_type.notification_title
      self.premium_type_notification_description = premium_type.notification_description
    end
    
    def as_json(options = nil)
        output = super
        output["user_token"] = self.user.token
        output["premium_type_token"] = self.premium_type.token
        output
    end
end