class Block < ActiveRecord::Base
    self.table_name = "block"
    
    GROUP_PRODUCT = 1
    GROUP_REWARD = 1
    
    belongs_to :company
    belongs_to :parent, class_name: Block, :foreign_key => "block_id"
    belongs_to :related, polymorphic: true
    has_many :blocks, dependent: :destroy
    has_many :translations, as: :related, dependent: :destroy
    
    validates :name, presence: true, length: { maximum: 200 }
    validates :group, presence: true, numericality: { only_integer: true }
    validates :layout, presence: true, numericality: { only_integer: true }
    validates :order, presence: true, numericality: { only_integer: true }
    
    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment_content_type :image, :content_type => %w(image/jpeg image/jpg image/png)
end
