class Photo < ActiveRecord::Base
    self.table_name = "photo"
    
    belongs_to :company
    belongs_to :related, polymorphic: true
    
    validates :company, presence: true
    validates :related, presence: true
    validates :code, presence: true, length: { maximum: 80 }
    validates :order, presence: true, numericality: { only_integer: true }
    validates :enabled, boolean: true
    
    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment_content_type :image, :content_type => %w(image/jpeg image/jpg image/png)
    
    def image_names
       [:image] 
    end
    
    def image_styles
       [:medium, :thumb] 
    end
end