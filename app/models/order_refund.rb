class OrderRefund < ActiveRecord::Base
    self.table_name = "order_refund"
    
    belongs_to :company
    belongs_to :order
    belongs_to :administrator
    
    validates :company, presence: true
    validates :order, presence: true
    validates :administrator, presence: true
    validates :value, presence: true, numericality: true
    validates :comments, length: { maximum: 65536 }
    
    before_save :before_save, :if => :new_record?
    after_save :after_save, :if => :new_record?
    
    def before_save
        if self.order.status == Order::STATUS_CANCELED
            raise BadRequestException, "Order already canceled"
        elsif self.order.status != Order::STATUS_CONFIRMED
            raise BadRequestException, "Order has not been confirmed"
        elsif self.order.invoiced_value < self.value
            raise BadRequestException, "Reimbursement of the value is greater than the value of the order"
        end
    end
    
    def after_save
       self.order.refund_and_save(self.value)
       
       cashflow_value = self.value * -1
       Cashflow.build_and_save(self.company, self.order.user, self.order, Cashflow::TYPE_ORDER_REFUND, cashflow_value)
       
       Notification.build_and_save(self.company, self.order.user, "order_refund", {order_token: self.order.token, value: self.value})
    end
end
