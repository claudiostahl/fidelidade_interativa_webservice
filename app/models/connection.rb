require 'digest/sha1'

class Connection < ActiveRecord::Base
  self.table_name = "connection"
  
  belongs_to :company
  belongs_to :related, polymorphic: true
  
  validates :company, presence: true
  validates :related, presence: true
  validates :device, presence: true, length: { maximum: 80 }
  validates :token, presence: true, length: { maximum: 80 }

  after_initialize :set_default, :if => :new_record?

  def set_default
    self.device = Digest::SHA1.hexdigest Time.now.to_f.to_s
    self.token = Digest::SHA1.hexdigest Random.rand.to_s
  end

  def self.build_default(company)
    connection = Connection.new
    connection.company = company
    connection
  end

end