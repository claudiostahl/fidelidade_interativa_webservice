class Vote < ActiveRecord::Base
    self.table_name = "vote"
    
    belongs_to :company
    belongs_to :user
    belongs_to :related, polymorphic: true
    
    validates :company, presence: true
    validates :user, presence: true
    validates :related, presence: true
    validates :vote, presence: true, numericality: true
    
    def vote=(vote)
        if vote.is_number?
            vote = vote.to_f if vote.is_a? String
            vote = 0 if vote <= 0
            vote = 10 if vote >= 10 
        end
        super
    end
end
