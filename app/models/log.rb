class Log < ActiveRecord::Base
    self.table_name = "log"
    
    belongs_to :company
    belongs_to :related, polymorphic: true
    
    validates :company, presence: true
    validates :related, presence: true
    validates :content, presence: true
    
    def self.build_and_save(company, related, content)
        log = Log.new
        log.company = company
        log.related = related
        log.content = content
        log.save! 
        log
    end
end
