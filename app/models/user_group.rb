class UserGroup < ActiveRecord::Base
    self.table_name = "user_group"
    
    belongs_to :company
    has_and_belongs_to_many :users, dependent: :destroy
    
    validates :name, presence: true, length: { maximum: 200 }
end
