class BooleanValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    can_nil = false
    if options.has_key? :allow_blank && options[:allow_blank]
      can_nil = true
    end
    unless value.kind_of?(TrueClass) || value.kind_of?(FalseClass) || (can_nil && value == nil)
      record.errors[attribute] << (options[:message] || "is not an boolean")
    end
  end
end