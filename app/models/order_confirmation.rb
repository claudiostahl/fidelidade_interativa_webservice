class OrderConfirmation < ActiveRecord::Base
    self.table_name = "order_confirmation"
    
    belongs_to :company
    belongs_to :order
    belongs_to :administrator
    
    validates :company, presence: true
    validates :order, presence: true
    validates :administrator, presence: true
    validates :comments, length: { maximum: 65536 }
    
    before_save :before_save, :if => :new_record?
    after_save :after_save, :if => :new_record?
    
    def before_save
        if self.order.status == Order::STATUS_CONFIRMED
            raise BadRequestException, "Order already confirmed"
        elsif self.order.status == Order::STATUS_CANCELED
            raise BadRequestException, "Order already canceled"
        end
    end
    
    def after_save
        self.order.confirmation_and_save
       
        Notification.build_and_save(self.company, self.order.user, "order_confirmed", {order_token: self.order.token})
    end
end