class CouponRead < ActiveRecord::Base
    self.table_name = "coupon_read"
    
    belongs_to :company
    belongs_to :coupon_group
    belongs_to :coupon
    belongs_to :user
    
    validates :company, presence: true
    validates :coupon_group, presence: true
    validates :coupon, presence: true
    validates :user, presence: true
    validates :value, presence: true, numericality: true
    
    after_save :after_save, :if => :new_record?
    
    def after_save
      if self.coupon.used >= self.coupon_group.limit
        raise BadRequestException, "Exceeded the limit" 
      end
      others = CouponRead.where(user: self.user).count
      if others > 0
        raise BadRequestException, "Coupon already used"
      end
      if self.coupon_group.validity < Time.now
        raise BadRequestException, "Coupon already ended" 
      end
      
      Cashflow.build_and_save(self.company, self.user, self.coupon_group, Cashflow::TYPE_COUPON, self.value)
      
      self.coupon.increment_and_save
    end
    
    def coupon=(coupon)
      super
      self.coupon_group = coupon.coupon_group
      self.value = coupon.coupon_group.value
    end
end
