class Message < ActiveRecord::Base
    self.table_name = "message"
    
    belongs_to :company
    belongs_to :administrator
    has_many :message_dispatches, dependent: :restrict_with_exception
    has_many :translations, as: :related, dependent: :destroy
    
    validates :company, presence: true
    validates :administrator, presence: true
    validates :link, presence: true, length: { maximum: 200 }
    validates :name, presence: true, length: { maximum: 200 }
    validates :description, presence: true, length: { maximum: 65536 }
    
    validates_datetime :publication_date
    
    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment_content_type :image, :content_type => %w(image/jpeg image/jpg image/png)
end
