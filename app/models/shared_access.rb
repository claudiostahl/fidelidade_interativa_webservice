class SharedAccess < ActiveRecord::Base
    self.table_name = "shared_access"
    
    belongs_to :company
    belongs_to :shared
    
    validates :company, presence: true
    validates :shared, presence: true
    validates :device, presence: true, length: { maximum: 80 }
    validates :ip, presence: true, length: { maximum: 80 }
    validates :value, presence: true, numericality: true
    
    before_save :before_save, :if => :new_record?
    after_save :after_save, :if => :new_record?
    
    def before_save
        self.value = Config.value_by_code(self.company, "shared_value").to_f
        # check limit
        limit_link = Config.value_by_code(self.company, "shared_limit_link").to_f
        limit_day = Config.value_by_code(self.company, "shared_limit_day").to_f
        count_link = SharedAccess.where(shared: self.shared).count
        count_day = SharedAccess.where(shared: self.shared, created_at: Time.now.midnight..(Time.now.midnight + 1.day)).count
        if count_link > limit_link
           raise BadRequestException, "Exceeded the limit link" 
        end
        if count_day > limit_day
           raise BadRequestException, "Exceeded the limit day" 
        end
    end
    
    def after_save
        Cashflow.build_and_save(self.company, self.shared.user, self, Cashflow::TYPE_SHARED, self.value)
        
        Notification.build_and_save(self.company, self.shared.user, "shared_access", {value: self.value})
    end
end
