class Product < ActiveRecord::Base
    self.table_name = "product"
    
    belongs_to :company
    has_many :blocks, as: :related, dependent: :destroy
    has_many :photos, as: :related, dependent: :destroy
    has_many :votes, as: :related, dependent: :destroy
    has_many :accesses, as: :related, dependent: :destroy
    has_many :shareds, as: :related, dependent: :nullify
    has_many :translations, as: :related, dependent: :destroy
    
    has_and_belongs_to_many(:related_products, :class_name => Product, :join_table => "product_related", :foreign_key => "parent_id", :association_foreign_key => "child_id", :dependent => :destroy)
    
    validates :company, presence: true
    validates :name, presence: true, length: { maximum: 200 }
    validates :slug, presence: true, uniqueness: {scope: :company}, length: { maximum: 200 }
    validates :short_description, length: { maximum: 200 }
    validates :description, length: { maximum: 65536 }
    validates :price, presence: true, numericality: true
    validates :special_price, presence: true, numericality: true
    validates :first_vote, presence: true, numericality: true
    validates :rate, presence: true, numericality: true
    validates :enabled, boolean: true
    
    def first_vote=(first_vote)
        super
        if first_vote.is_number?
            first_vote = first_vote.to_f if first_vote.is_a? String
            first_vote = 0 if first_vote <= 0
            first_vote = 10 if first_vote >= 10 
        end
        total = first_vote
        count = 1
        votes = Vote.where(related: self, related_type: "Product")
        for vote in votes
            total+=vote.vote
            count+=1
        end
        self.rate = total/count
    end
    
end