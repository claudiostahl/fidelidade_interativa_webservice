class Notification < ActiveRecord::Base
    self.table_name = "notification"
    
    belongs_to :company
    belongs_to :user
    
    validates :company, presence: true
    validates :user, presence: true
    validates :title, presence: true, length: { maximum: 200 }
    validates :description, presence: true, length: { maximum: 65536 }
    validates :read, boolean: true
    validates :arquived, boolean: true
    
    def build_and_save(company, user, code, vars)
        title = Config.value_by_code(company, "notification_" + code + "_title")
        description = Config.value_by_code(company, "notification_" + code + "_description")
        if !user.language.blank?
            title_translated = Config.value_by_code(company, "notification_" + code + "_title_" + user.language)
            description_translated = Config.value_by_code(company, "notification_" + code + "_description_" + user.language)
            if !title_translated.blank?
               title = title_translated 
            end
            if !description_translated.blank?
               description = description_translated 
            end
        end
        vars.each do |key, value|
           title = title.gsub key, value 
           description = description.gsub key, value 
        end
        if !title.blank? && !description.blank?
            Notification.create(company: company, user: user, title: title, description: description).save!
        end
    end
    
    def build_premium_and_save(premium)
        title = premium.premium_type.notification_title
        description = premium.premium_type.notification_description
        if !premium.user.language.blank?
            language = Language.where(code: premium.user.language).first
            if language
                title_translated = Translation.value_by_code(premium.company, "notification_title", language, premium)
                title_translated = Translation.value_by_code(premium.company, "notification_titleription", language, premium)
                if !title_translated.blank?
                   title = title_translated 
                end
                if !description_translated.blank?
                   description = description_translated 
                end
            end
        end
        vars = {value: premium.value}
        vars.each do |key, value|
           title = title.gsub key, value 
           description = description.gsub key, value 
        end
        if !title.blank? && !description.blank?
            Notification.create(company: premium.company, user: premium.user, title: title, description: description).save!
        end
    end
    
end
