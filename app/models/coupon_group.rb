class CouponGroup < ActiveRecord::Base
    self.table_name = "coupon_group"
    
    belongs_to :company
    belongs_to :administrator
    
    has_many :coupons, dependent: :destroy
    has_many :coupon_reads, dependent: :restrict_with_exception
    
    validates :company, presence: true
    validates :administrator, presence: true
    validates :name, presence: true, length: { maximum: 200 }
    validates :description, length: { maximum: 65536 }
    validates :validity, presence: true
    validates :order, numericality: { only_integer: true, allow_blank: true }
    validates :limit, presence: true, numericality: { only_integer: true }
    validates :enabled, boolean: true
end
