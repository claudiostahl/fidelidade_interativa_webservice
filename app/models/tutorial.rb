class Tutorial < ActiveRecord::Base
    self.table_name = "tutorial"
    
    belongs_to :company
    has_many :accesses, as: :related, dependent: :destroy
    has_many :translations, as: :related, dependent: :destroy
    
    validates :company, presence: true
    validates :name, presence: true, length: { maximum: 200 }
    validates :image, presence: true
    validates :order, numericality: { only_integer: true, allow_blank: true }
    validates :enabled, boolean: true
    
    has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }
    validates_attachment_content_type :image, :content_type => %w(image/jpeg image/jpg image/png)
end
