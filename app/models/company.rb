class Company < ActiveRecord::Base
  self.table_name = "company"
  
  has_and_belongs_to_many :administrators, dependent: :destroy
  has_and_belongs_to_many :packages, dependent: :destroy
  has_and_belongs_to_many :languages, dependent: :destroy
  has_many :users, dependent: :restrict_with_exception
  
  validates :code, presence: true, uniqueness: true, length: { maximum: 200 }
  validates :name, presence: true, uniqueness: true, length: { maximum: 200 }
  validates :token, presence: true, uniqueness: true, length: { maximum: 200 }
  validates :enabled_backend, boolean: true
  validates :enabled_frontend, boolean: true
  validates :enabled_api, boolean: true
  
  after_initialize :set_default, :if => :new_record?
  after_save :after_save, :if => :new_record?
  
  def set_default
    self.token = Digest::SHA1.hexdigest Time.now.to_f.to_s
  end
  
  def after_save
    # general
    Config.create_by_code(self, "frontend_enabled", true)
    Config.create_by_code(self, "api_enabled", true)
    Config.create_by_code(self, "coupon_enabled", true)
    Config.create_by_code(self, "order_enabled", true)
    # cashflow
    Config.create_by_code(self, "cashflow_type_order_new", "New order")
    Config.create_by_code(self, "cashflow_type_order_refund", "Refund order")
    Config.create_by_code(self, "cashflow_type_order_cancel", "Cancel order")
    Config.create_by_code(self, "cashflow_type_coupon", "Coupon")
    Config.create_by_code(self, "cashflow_type_premium", "Premium")
    # notification
    Config.create_by_code(self, "notification_order_confirmed_title", "Order confirmed")
    Config.create_by_code(self, "notification_order_confirmed_description", "The order {order_token} confirmed with succees. Thank you.")
    Config.create_by_code(self, "notification_order_refund_title", "Order refund")
    Config.create_by_code(self, "notification_order_refund_description", "...{order_token}...{value}...")
    Config.create_by_code(self, "notification_order_canceled_title", "Order canceled")
    Config.create_by_code(self, "notification_order_canceled_description", "...{order_token}...{value}...")
    Config.create_by_code(self, "notification_shared_access_title", "Shared")
    Config.create_by_code(self, "notification_shared_access_description", "...{value}...")
    # shared
    Config.create_by_code(self, "shared_value", 1)
    Config.create_by_code(self, "shared_limit_link", 20)
    Config.create_by_code(self, "shared_limit_day", 100)
    Config.create_by_code(self, "shared_redirect_url", "http://interagynet.com.br")
  end
end