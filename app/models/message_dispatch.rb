class MessageDispatch < ActiveRecord::Base
    self.table_name = "message_dispatch"
    
    belongs_to :company
    belongs_to :message
    belongs_to :user
    
    validates :company, presence: true
    validates :message, presence: true
    validates :user, presence: true
    validates :sent, boolean: true
    
    validates_datetime :sent_at, allow_nil: true
    
    def self.build_from_user(company, user)
        message_dispatch = MessageDispatch.new
        message_dispatch.company = company
        message_dispatch.user = user
        message_dispatch
    end
end