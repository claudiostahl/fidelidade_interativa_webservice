class Post < ActiveRecord::Base
    self.table_name = "post"
    
    belongs_to :company
    has_many :accesses, as: :related, dependent: :destroy
    has_many :shareds, as: :related, dependent: :nullify
    has_many :translations, as: :related, dependent: :destroy
    
    validates :company, presence: true
    validates :title, presence: true, length: { maximum: 200 }
    validates :slug, presence: true, uniqueness: {scope: :company}, length: { maximum: 200 }
    validates :description, presence: true, length: { maximum: 65536 }
    validates :content, presence: true, length: { maximum: 65536 }
    validates :publication_date, presence: true
    validates :enabled, boolean: true
    
    validates_datetime :publication_date
end