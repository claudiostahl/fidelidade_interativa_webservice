class Cashflow < ActiveRecord::Base
    self.table_name = "cashflow"
    
    TYPE_ORDER_NEW = 1
    TYPE_ORDER_REFUND = 2
    TYPE_ORDER_CANCELED = 3
    TYPE_COUPON = 4
    TYPE_PREMIUM = 5
    TYPE_SHARED = 6
    
    belongs_to :company
    belongs_to :user
    belongs_to :related, polymorphic: true
    
    validates :company, presence: true
    validates :user, presence: true
    validates :related, presence: true
    validates :type_flow, presence: true, inclusion: {:in => 1..5}
    validates :value, presence: true, numericality: true
    
    after_save :after_save
    
    def after_save
      self.user.credit += self.value
      self.user.save!
    end
    
    def self.build_and_save(company, user, related, type, value)
      cashflow = Cashflow.create(company: company, user: user, related: related, type_flow: type, value: value)
      cashflow.save!
      cashflow
    end
end
