class Language < ActiveRecord::Base
    self.table_name = "language"
    
    validates :name, presence: true, uniqueness: true, length: { maximum: 80 }
    validates :abbreviation, presence: true, uniqueness: true, length: { maximum: 10 }
    
    def related_attribute
       self.abbreviation 
    end
end
