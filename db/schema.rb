# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151007233800) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "access", ["company_id"], name: "index_access_on_company_id", using: :btree
  add_index "access", ["related_type", "related_id"], name: "index_access_on_related_type_and_related_id", using: :btree
  add_index "access", ["user_id"], name: "index_access_on_user_id", using: :btree

  create_table "administrator", force: :cascade do |t|
    t.integer  "language_id"
    t.string   "name",          limit: 200
    t.string   "email",         limit: 200
    t.string   "password_hash", limit: 200
    t.string   "recover_token", limit: 200
    t.integer  "timezone",      limit: 2
    t.boolean  "is_root",                   default: false
    t.boolean  "enabled"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "administrator", ["language_id"], name: "index_administrator_on_language_id", using: :btree

  create_table "administrator_company", id: false, force: :cascade do |t|
    t.integer "company_id",       null: false
    t.integer "administrator_id", null: false
  end

  add_index "administrator_company", ["administrator_id"], name: "index_administrator_company_on_administrator_id", using: :btree
  add_index "administrator_company", ["company_id"], name: "index_administrator_company_on_company_id", using: :btree

  create_table "administrator_resource", id: false, force: :cascade do |t|
    t.integer "administrator_id", null: false
    t.integer "resource_id",      null: false
  end

  add_index "administrator_resource", ["administrator_id"], name: "index_administrator_resource_on_administrator_id", using: :btree
  add_index "administrator_resource", ["resource_id"], name: "index_administrator_resource_on_resource_id", using: :btree

  create_table "banner", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "administrator_id"
    t.string   "link",               limit: 200
    t.string   "name",               limit: 200
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "enabled"
    t.integer  "order",              limit: 8
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "banner", ["administrator_id"], name: "index_banner_on_administrator_id", using: :btree
  add_index "banner", ["company_id"], name: "index_banner_on_company_id", using: :btree

  create_table "block", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "block_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.string   "name",               limit: 200
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "group",              limit: 2
    t.integer  "layout",             limit: 2
    t.integer  "order",              limit: 8
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "block", ["block_id"], name: "index_block_on_block_id", using: :btree
  add_index "block", ["company_id"], name: "index_block_on_company_id", using: :btree
  add_index "block", ["related_type", "related_id"], name: "index_block_on_related_type_and_related_id", using: :btree

  create_table "cashflow", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.integer  "type_flow",    limit: 2
    t.decimal  "value",                  precision: 20, scale: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "cashflow", ["company_id"], name: "index_cashflow_on_company_id", using: :btree
  add_index "cashflow", ["related_type", "related_id"], name: "index_cashflow_on_related_type_and_related_id", using: :btree
  add_index "cashflow", ["user_id"], name: "index_cashflow_on_user_id", using: :btree

  create_table "company", force: :cascade do |t|
    t.string   "code",             limit: 200
    t.string   "name",             limit: 200
    t.string   "token",            limit: 200
    t.boolean  "enabled_backend",              default: true
    t.boolean  "enabled_frontend",             default: true
    t.boolean  "enabled_api",                  default: true
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  create_table "company_language", id: false, force: :cascade do |t|
    t.integer "company_id",  null: false
    t.integer "language_id", null: false
  end

  add_index "company_language", ["company_id"], name: "index_company_language_on_company_id", using: :btree
  add_index "company_language", ["language_id"], name: "index_company_language_on_language_id", using: :btree

  create_table "company_package", id: false, force: :cascade do |t|
    t.integer "company_id", null: false
    t.integer "package_id", null: false
  end

  add_index "company_package", ["company_id"], name: "index_company_package_on_company_id", using: :btree
  add_index "company_package", ["package_id"], name: "index_company_package_on_package_id", using: :btree

  create_table "config", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "code",           limit: 200
    t.string   "value_string",   limit: 200
    t.text     "value_text"
    t.decimal  "value_number",               precision: 20, scale: 4
    t.datetime "value_datetime"
    t.boolean  "value_boolean"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  add_index "config", ["company_id"], name: "index_config_on_company_id", using: :btree

  create_table "connection", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.string   "device",       limit: 80
    t.string   "token",        limit: 80
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "connection", ["company_id"], name: "index_connection_on_company_id", using: :btree
  add_index "connection", ["related_type", "related_id"], name: "index_connection_on_related_type_and_related_id", using: :btree

  create_table "coupon", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "coupon_group_id"
    t.string   "token",           limit: 80
    t.integer  "used",            limit: 8,  default: 0
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "coupon", ["company_id"], name: "index_coupon_on_company_id", using: :btree
  add_index "coupon", ["coupon_group_id"], name: "index_coupon_on_coupon_group_id", using: :btree

  create_table "coupon_group", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "administrator_id"
    t.string   "name",             limit: 200
    t.text     "description"
    t.decimal  "value",                        precision: 20, scale: 4
    t.datetime "validity"
    t.integer  "limit",            limit: 8
    t.integer  "order",            limit: 8
    t.boolean  "enabled"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
  end

  add_index "coupon_group", ["administrator_id"], name: "index_coupon_group_on_administrator_id", using: :btree
  add_index "coupon_group", ["company_id"], name: "index_coupon_group_on_company_id", using: :btree

  create_table "coupon_read", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "coupon_group_id"
    t.integer  "coupon_id"
    t.integer  "user_id"
    t.decimal  "value",           precision: 20, scale: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "coupon_read", ["company_id"], name: "index_coupon_read_on_company_id", using: :btree
  add_index "coupon_read", ["coupon_group_id"], name: "index_coupon_read_on_coupon_group_id", using: :btree
  add_index "coupon_read", ["coupon_id"], name: "index_coupon_read_on_coupon_id", using: :btree
  add_index "coupon_read", ["user_id"], name: "index_coupon_read_on_user_id", using: :btree

  create_table "developer", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "name",       limit: 200
    t.string   "email",      limit: 200
    t.string   "token",      limit: 200
    t.boolean  "enabled"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "developer", ["company_id"], name: "index_developer_on_company_id", using: :btree

  create_table "developer_resource", id: false, force: :cascade do |t|
    t.integer "developer_id", null: false
    t.integer "resource_id",  null: false
  end

  add_index "developer_resource", ["developer_id"], name: "index_developer_resource_on_developer_id", using: :btree
  add_index "developer_resource", ["resource_id"], name: "index_developer_resource_on_resource_id", using: :btree

  create_table "language", force: :cascade do |t|
    t.string   "name",         limit: 80
    t.string   "abbreviation", limit: 10
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "log", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.text     "content"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "log", ["company_id"], name: "index_log_on_company_id", using: :btree
  add_index "log", ["related_type", "related_id"], name: "index_log_on_related_type_and_related_id", using: :btree

  create_table "menu", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "title",              limit: 200
    t.string   "url",                limit: 200
    t.integer  "order",              limit: 8
    t.text     "content"
    t.boolean  "enabled"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "menu", ["company_id"], name: "index_menu_on_company_id", using: :btree

  create_table "message", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "administrator_id"
    t.string   "link",               limit: 200
    t.string   "name",               limit: 200
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "publication_date"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "message", ["administrator_id"], name: "index_message_on_administrator_id", using: :btree
  add_index "message", ["company_id"], name: "index_message_on_company_id", using: :btree

  create_table "message_dispatch", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "message_id"
    t.integer  "user_id"
    t.boolean  "sent",       default: false
    t.datetime "sent_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "message_dispatch", ["company_id"], name: "index_message_dispatch_on_company_id", using: :btree
  add_index "message_dispatch", ["message_id"], name: "index_message_dispatch_on_message_id", using: :btree
  add_index "message_dispatch", ["user_id"], name: "index_message_dispatch_on_user_id", using: :btree

  create_table "notification", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.string   "title",       limit: 200
    t.text     "description"
    t.boolean  "read",                    default: false
    t.boolean  "arquived",                default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "notification", ["company_id"], name: "index_notification_on_company_id", using: :btree
  add_index "notification", ["user_id"], name: "index_notification_on_user_id", using: :btree

  create_table "order", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "administrator_id"
    t.integer  "reward_id"
    t.string   "token",                    limit: 80
    t.text     "comments"
    t.string   "reward_name",              limit: 200
    t.string   "reward_slug",              limit: 200
    t.string   "reward_short_description", limit: 200
    t.text     "reward_description"
    t.text     "reward_rule_description"
    t.decimal  "value",                                precision: 20, scale: 4
    t.decimal  "price",                                precision: 20, scale: 4
    t.decimal  "invoiced_value",                       precision: 20, scale: 4
    t.integer  "status",                   limit: 2
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
  end

  add_index "order", ["administrator_id"], name: "index_order_on_administrator_id", using: :btree
  add_index "order", ["company_id"], name: "index_order_on_company_id", using: :btree
  add_index "order", ["reward_id"], name: "index_order_on_reward_id", using: :btree
  add_index "order", ["user_id"], name: "index_order_on_user_id", using: :btree

  create_table "order_canceled", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "order_id"
    t.integer  "administrator_id"
    t.boolean  "system",           default: false
    t.text     "comments"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "order_canceled", ["administrator_id"], name: "index_order_canceled_on_administrator_id", using: :btree
  add_index "order_canceled", ["company_id"], name: "index_order_canceled_on_company_id", using: :btree
  add_index "order_canceled", ["order_id"], name: "index_order_canceled_on_order_id", using: :btree

  create_table "order_confirmation", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "order_id"
    t.integer  "administrator_id"
    t.text     "comments"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "order_confirmation", ["administrator_id"], name: "index_order_confirmation_on_administrator_id", using: :btree
  add_index "order_confirmation", ["company_id"], name: "index_order_confirmation_on_company_id", using: :btree
  add_index "order_confirmation", ["order_id"], name: "index_order_confirmation_on_order_id", using: :btree

  create_table "order_refund", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "order_id"
    t.integer  "administrator_id"
    t.decimal  "value",            precision: 20, scale: 4
    t.text     "comments"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "order_refund", ["administrator_id"], name: "index_order_refund_on_administrator_id", using: :btree
  add_index "order_refund", ["company_id"], name: "index_order_refund_on_company_id", using: :btree
  add_index "order_refund", ["order_id"], name: "index_order_refund_on_order_id", using: :btree

  create_table "package", force: :cascade do |t|
    t.string   "code",       limit: 200
    t.string   "name",       limit: 200
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "photo", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.string   "code",               limit: 80
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "order",              limit: 8
    t.boolean  "enabled"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "photo", ["company_id"], name: "index_photo_on_company_id", using: :btree
  add_index "photo", ["related_type", "related_id"], name: "index_photo_on_related_type_and_related_id", using: :btree

  create_table "post", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "title",            limit: 200
    t.string   "slug",             limit: 200
    t.text     "description"
    t.text     "content"
    t.datetime "publication_date"
    t.boolean  "enabled"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "post", ["company_id"], name: "index_post_on_company_id", using: :btree

  create_table "premium", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "premium_type_id"
    t.integer  "developer_id"
    t.string   "premium_type_name",                     limit: 200
    t.text     "premium_type_description"
    t.string   "premium_type_notification_title",       limit: 200
    t.text     "premium_type_notification_description"
    t.decimal  "value",                                             precision: 20, scale: 4
    t.string   "campaign",                              limit: 200
    t.datetime "created_at",                                                                 null: false
    t.datetime "updated_at",                                                                 null: false
  end

  add_index "premium", ["company_id"], name: "index_premium_on_company_id", using: :btree
  add_index "premium", ["developer_id"], name: "index_premium_on_developer_id", using: :btree
  add_index "premium", ["premium_type_id"], name: "index_premium_on_premium_type_id", using: :btree
  add_index "premium", ["user_id"], name: "index_premium_on_user_id", using: :btree

  create_table "premium_type", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "token",                    limit: 200
    t.string   "name",                     limit: 200
    t.text     "description"
    t.string   "notification_title",       limit: 200
    t.text     "notification_description"
    t.decimal  "value",                                precision: 20, scale: 4
    t.integer  "limit",                    limit: 8
    t.integer  "limit_user",               limit: 8
    t.datetime "validity"
    t.boolean  "enabled"
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
  end

  add_index "premium_type", ["company_id"], name: "index_premium_type_on_company_id", using: :btree

  create_table "product", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "name",              limit: 200
    t.string   "slug",              limit: 200
    t.string   "short_description", limit: 200
    t.text     "description"
    t.decimal  "price",                         precision: 20, scale: 4
    t.decimal  "special_price",                 precision: 20, scale: 4
    t.decimal  "first_vote",                    precision: 20, scale: 4
    t.decimal  "rate",                          precision: 20, scale: 4
    t.boolean  "enabled"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
  end

  add_index "product", ["company_id"], name: "index_product_on_company_id", using: :btree

  create_table "product_related", id: false, force: :cascade do |t|
    t.integer "parent_id", null: false
    t.integer "child_id",  null: false
  end

  add_index "product_related", ["child_id"], name: "index_product_related_on_child_id", using: :btree
  add_index "product_related", ["parent_id"], name: "index_product_related_on_parent_id", using: :btree

  create_table "resource", force: :cascade do |t|
    t.string   "code",       limit: 200
    t.string   "name",       limit: 200
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "reward", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_group_id"
    t.string   "name",              limit: 200
    t.string   "slug",              limit: 200
    t.string   "short_description", limit: 200
    t.text     "description"
    t.text     "rule_description"
    t.decimal  "value",                         precision: 20, scale: 4
    t.decimal  "price",                         precision: 20, scale: 4
    t.decimal  "first_vote",                    precision: 20, scale: 4
    t.decimal  "rate",                          precision: 20, scale: 4
    t.datetime "publication_date"
    t.integer  "limit",             limit: 8
    t.integer  "limit_user",        limit: 8
    t.datetime "date_start"
    t.datetime "date_finish"
    t.boolean  "enabled"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
  end

  add_index "reward", ["company_id"], name: "index_reward_on_company_id", using: :btree
  add_index "reward", ["user_group_id"], name: "index_reward_on_user_group_id", using: :btree

  create_table "reward_rule", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "reward_id"
    t.integer  "week_day",    limit: 2
    t.time     "hour_start"
    t.time     "hour_finish"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "reward_rule", ["company_id"], name: "index_reward_rule_on_company_id", using: :btree
  add_index "reward_rule", ["reward_id"], name: "index_reward_rule_on_reward_id", using: :btree

  create_table "shared", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.string   "token",        limit: 80
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "shared", ["company_id"], name: "index_shared_on_company_id", using: :btree
  add_index "shared", ["related_type", "related_id"], name: "index_shared_on_related_type_and_related_id", using: :btree
  add_index "shared", ["user_id"], name: "index_shared_on_user_id", using: :btree

  create_table "shared_access", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "shared_id"
    t.string   "device",     limit: 80
    t.string   "ip",         limit: 80
    t.decimal  "value",                 precision: 20, scale: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  add_index "shared_access", ["company_id"], name: "index_shared_access_on_company_id", using: :btree
  add_index "shared_access", ["shared_id"], name: "index_shared_access_on_shared_id", using: :btree

  create_table "translation", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "language_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.string   "code",               limit: 80
    t.string   "value_string",       limit: 200
    t.text     "value_text"
    t.decimal  "value_number",                   precision: 20, scale: 4
    t.datetime "value_datetime"
    t.boolean  "value_boolean"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
  end

  add_index "translation", ["company_id"], name: "index_translation_on_company_id", using: :btree
  add_index "translation", ["language_id"], name: "index_translation_on_language_id", using: :btree
  add_index "translation", ["related_type", "related_id"], name: "index_translation_on_related_type_and_related_id", using: :btree

  create_table "tutorial", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "name",               limit: 200
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "order",              limit: 8
    t.boolean  "enabled"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "tutorial", ["company_id"], name: "index_tutorial_on_company_id", using: :btree

  create_table "user", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "token",            limit: 200
    t.string   "name",             limit: 200
    t.string   "email",            limit: 200
    t.string   "password_hash",    limit: 200
    t.string   "facebook",         limit: 200
    t.string   "recover_token",    limit: 200
    t.decimal  "credit",                       precision: 20, scale: 4, default: 0.0
    t.boolean  "can_notification",                                      default: true
    t.datetime "last_access_at"
    t.datetime "last_buy_at"
    t.string   "language",         limit: 10
    t.integer  "timezone",         limit: 2
    t.boolean  "enabled"
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
  end

  add_index "user", ["company_id"], name: "index_user_on_company_id", using: :btree

  create_table "user_group", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "name",       limit: 200
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "user_group", ["company_id"], name: "index_user_group_on_company_id", using: :btree

  create_table "user_user_group", id: false, force: :cascade do |t|
    t.integer "user_id",       null: false
    t.integer "user_group_id", null: false
  end

  add_index "user_user_group", ["user_group_id"], name: "index_user_user_group_on_user_group_id", using: :btree
  add_index "user_user_group", ["user_id"], name: "index_user_user_group_on_user_id", using: :btree

  create_table "vote", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "user_id"
    t.integer  "related_id"
    t.string   "related_type"
    t.decimal  "vote",         precision: 20, scale: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "vote", ["company_id"], name: "index_vote_on_company_id", using: :btree
  add_index "vote", ["related_type", "related_id"], name: "index_vote_on_related_type_and_related_id", using: :btree
  add_index "vote", ["user_id"], name: "index_vote_on_user_id", using: :btree

end
