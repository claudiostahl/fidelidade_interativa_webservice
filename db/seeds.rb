# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

languages = Language.create([
    {name: "Português", abbreviation: "pt_BR"},
    {name: "English", abbreviation: "en"}
])

packages = Package.create([
    {code: "default", name: "Default"},
    {code: "product", name: "Product"},
    {code: "post", name: "Post"},
    {code: "reward", name: "Reward"},
    {code: "message", name: "Message"},
    {code: "menu", name: "Menu"},
    {code: "banner", name: "Banner"},
    {code: "developer", name: "Developer"}
])

resources = Resource.create([
    {code: "default", name: "Default"},
    {code: "administrator", name: "Administrator"},
    {code: "user", name: "User"},
    {code: "user_group", name: "User Group"},
    {code: "product", name: "Product"},
    {code: "post", name: "Post"},
    {code: "reward", name: "Reward"},
    {code: "message", name: "Message"},
    {code: "block", name: "Block"},
    {code: "coupon", name: "Coupon"},
    {code: "coupon_group", name: "Coupon Group"},
    {code: "tutorial", name: "Tutorial"},
    {code: "menu", name: "Menu"},
    {code: "premium", name: "Premium"},
    {code: "premium_type", name: "Premium Type"},
    {code: "banner", name: "Banner"},
    {code: "order", name: "Order"},
    {code: "cashflow", name: "Cashflow"},
    {code: "coupon_read", name: "Coupon Read"},
    {code: "vote", name: "Vote"},
    {code: "notification", name: "Notification"},
    {code: "shared", name: "Shared"},
    {code: "access", name: "Access"},
    {code: "config", name: "Config"},
    {code: "information", name: "Information"},
    {code: "translation", name: "Translation"},
    {code: "developer", name: "Developer"}
])

company = Company.create(
    code: "interagynet",
    name: "Interagynet",
    enabled_backend: true,
    enabled_frontend: true,
    enabled_api: true,
    packages: [packages[0], packages[1], packages[2], packages[3], packages[4], packages[5], packages[6], packages[7]]
)

Administrator.create(
    companies: [company],
    language: languages.first,
    name: "Root",
    email: "root@interagynet.com.br",
    password: "102030",
    timezone: -3,
    is_root: true,
    enabled: true
)