class CreateAll < ActiveRecord::Migration
  def change

    create_table :company do |t|
      t.string :code, limit: 200
      t.string :name, limit: 200
      t.string :token, limit: 200
      t.boolean :enabled_backend, default: true
      t.boolean :enabled_frontend, default: true
      t.boolean :enabled_api, default: true
      t.timestamps null: false
    end

    create_table :user do |t|
      t.references :company, index: true
      t.string :token, limit: 200
      t.string :name, limit: 200
      t.string :email, limit: 200
      t.string :password_hash, limit: 200
      t.string :facebook, limit: 200
      t.string :recover_token, limit: 200
      t.decimal :credit, precision: 20, scale: 4, default: 0
      t.boolean :can_notification, default: true
      t.datetime :last_access_at
      t.datetime :last_buy_at
      t.string :language, limit: 10
      t.integer :timezone, limit: 1
      t.boolean :enabled
      t.timestamps null: false
    end

    create_table :user_group do |t|
      t.references :company, index: true
      t.string :name, limit: 200
      t.timestamps null: false
    end

    create_join_table :user, :user_group do |t|
      t.index :user_id
      t.index :user_group_id
    end

    create_table :administrator do |t|
      t.references :language, index: true
      t.string :name, limit: 200
      t.string :email, limit: 200
      t.string :password_hash, limit: 200
      t.string :recover_token, limit: 200
      t.integer :timezone, limit: 1
      t.boolean :is_root, default: false
      t.boolean :enabled
      t.timestamps null: false
    end

    create_join_table :company, :administrator do |t|
      t.index :company_id
      t.index :administrator_id
    end
    
    create_table :developer do |t|
      t.references :company, index: true
      t.string :name, limit: 200
      t.string :email, limit: 200
      t.string :token, limit: 200
      t.boolean :enabled
      t.timestamps null: false
    end

    create_table :config do |t|
      t.references :company, index: true
      t.string :code, limit: 200
      t.string :value_string, limit: 200
      t.text :value_text, limit: 65536
      t.decimal :value_number, precision: 20, scale: 4
      t.datetime :value_datetime
      t.boolean :value_boolean
      t.timestamps null: false
    end

    create_table :log do |t|
      t.references :company, index: true
      t.references :related, polymorphic: true, index: true
      t.text :content, limit: 65536
      t.timestamps null: false
    end

    create_table :resource do |t|
      t.string :code, limit: 200
      t.string :name, limit: 200
      t.timestamps null: false
    end
    
    create_table :package do |t|
      t.string :code, limit: 200
      t.string :name, limit: 200
      t.timestamps null: false
    end
    
    create_join_table :developer, :resource do |t|
      t.index :developer_id
      t.index :resource_id
    end

    create_join_table :administrator, :resource do |t|
      t.index :administrator_id
      t.index :resource_id
    end
    
    create_join_table :company, :package do |t|
      t.index :company_id
      t.index :package_id
    end
    
    create_join_table :company, :language do |t|
      t.index :company_id
      t.index :language_id
    end

    create_table :product do |t|
      t.references :company, index: true
      t.string :name, limit: 200
      t.string :slug, limit: 200
      t.string :short_description, limit: 200
      t.text :description, limit: 65536
      t.decimal :price, precision: 20, scale: 4
      t.decimal :special_price, precision: 20, scale: 4
      t.decimal :first_vote, precision: 20, scale: 4, defaut: 0
      t.decimal :rate, precision: 20, scale: 4, defaut: 0
      t.boolean :enabled
      t.timestamps null: false
    end

    create_table :product_related, :id => false do |t|
      t.integer :parent_id, :null => false, index: true
      t.integer :child_id, :null => false, index: true
    end

    create_table :post do |t|
      t.references :company, index: true
      t.string :title, limit: 200
      t.string :slug, limit: 200
      t.text :description, limit: 65536
      t.text :content, limit: 65536
      t.datetime :publication_date
      t.boolean :enabled
      t.timestamps null: false
    end

    create_table :block do |t|
      t.references :company, index: true
      t.references :block, index: true
      t.references :related, polymorphic: true, index: true
      t.string :name, limit: 200
      t.attachment :image
      t.integer :group, limit: 1
      t.integer :layout, limit: 1
      t.integer :order, limit: 8
      t.timestamps null: false
    end

    create_table :tutorial do |t|
      t.references :company, index: true
      t.string :name, limit: 200
      t.attachment :image
      t.integer :order, limit: 8
      t.boolean :enabled
      t.timestamps null: false
    end

    create_table :menu do |t|
      t.references :company, index: true
      t.attachment :image
      t.string :title, limit: 200
      t.string :url, limit: 200
      t.integer :order, limit: 8
      t.text :content, limit: 65536
      t.boolean :enabled
      t.timestamps null: false
    end

    create_table :reward do |t|
      t.references :company, index: true
      t.references :user_group, index: true
      t.string :name, limit: 200
      t.string :slug, limit: 200
      t.string :short_description, limit: 200
      t.text :description, limit: 65536
      t.text :rule_description, limit: 65536
      t.decimal :value, precision: 20, scale: 4
      t.decimal :price, precision: 20, scale: 4
      t.decimal :first_vote, precision: 20, scale: 4, defaut: 0
      t.decimal :rate, precision: 20, scale: 4, defaut: 0
      t.datetime :publication_date
      t.integer :limit, limit: 8
      t.integer :limit_user, limit: 8
      t.datetime :date_start
      t.datetime :date_finish
      t.boolean :enabled
      t.timestamps null: false
    end
    
    create_table :reward_rule do |t|
      t.references :company, index: true
      t.references :reward, index: true
      t.integer :week_day, limit: 1
      t.time :hour_start
      t.time :hour_finish
      t.timestamps null: false
    end

    create_table :vote do |t|
      t.references :company, index: true
      t.references :user, index: true
      t.references :related, polymorphic: true, index: true
      t.decimal :vote, precision: 20, scale: 4
      t.timestamps null: false
    end

    create_table :notification do |t| # pendente
      t.references :company, index: true
      t.references :user, index: true
      t.string :title, limit: 200
      t.text :description, limit: 65536
      t.boolean :read, default: false
      t.boolean :arquived, default: false
      t.timestamps null: false
    end
    
    create_table :coupon_read do |t|
      t.references :company, index: true
      t.references :coupon_group, index: true
      t.references :coupon, index: true
      t.references :user, index: true
      t.decimal :value, precision: 20, scale: 4
      t.timestamps null: false
    end

    create_table :coupon do |t|
      t.references :company, index: true
      t.references :coupon_group, index: true
      t.string :token, limit: 80
      t.integer :used, limit: 8, default: 0
      t.timestamps null: false
    end

    create_table :coupon_group do |t|
      t.references :company, index: true
      t.references :administrator, index: true
      t.string :name, limit: 200
      t.text :description, limit: 65536
      t.decimal :value, precision: 20, scale: 4
      t.datetime :validity
      t.integer :limit, limit: 8
      t.integer :order, limit: 8
      t.boolean :enabled
      t.timestamps null: false
    end

    create_table :cashflow do |t|
      t.references :company, index: true
      t.references :user, index: true
      t.references :related, polymorphic: true, index: true
      t.integer :type_flow, limit: 1
      t.decimal :value, precision: 20, scale: 4
      t.timestamps null: false
    end

    create_table :premium do |t|
      t.references :company, index: true
      t.references :user, index: true
      t.references :premium_type, index: true
      t.references :developer, index: true
      t.string :premium_type_name, limit: 200
      t.text :premium_type_description, limit: 65536
      t.string :premium_type_notification_title, limit: 200
      t.text :premium_type_notification_description, limit: 65536
      t.decimal :value, precision: 20, scale: 4
      t.string :campaign, limit: 200
      t.timestamps null: false
    end
    
    create_table :premium_type do |t|
      t.references :company, index: true
      t.string :token, limit: 200
      t.string :name, limit: 200
      t.text :description, limit: 65536
      t.string :notification_title, limit: 200
      t.text :notification_description, limit: 65536
      t.decimal :value, precision: 20, scale: 4
      t.integer :limit, limit: 8
      t.integer :limit_user, limit: 8
      t.datetime :validity
      t.boolean :enabled
      t.timestamps null: false
    end

    create_table :banner do |t|
      t.references :company, index: true
      t.references :administrator, index: true
      t.string :link, limit: 200
      t.string :name, limit: 200
      t.attachment :image
      t.boolean :enabled
      t.integer :order, limit: 8
      t.timestamps null: false
    end

    create_table :message do |t|
      t.references :company, index: true
      t.references :administrator, index: true
      t.string :link, limit: 200
      t.string :name, limit: 200
      t.text :description, limit: 65536
      t.attachment :image
      t.datetime :publication_date
      t.timestamps null: false
    end

    create_table :message_dispatch do |t|
      t.references :company, index: true
      t.references :message, index: true
      t.references :user, index: true
      t.boolean :sent, default: false
      t.datetime :sent_at
      t.timestamps null: false
    end
    
    create_table :order do |t|
      t.references :company, index: true
      t.references :user, index: true
      t.references :administrator, index: true
      t.references :reward, index: true
      t.string :token, limit: 80
      t.text :comments, limit: 65536
      t.string :reward_name, limit: 200
      t.string :reward_slug, limit: 200
      t.string :reward_short_description, limit: 200
      t.text :reward_description, limit: 65536
      t.text :reward_rule_description, limit: 65536
      t.decimal :value, precision: 20, scale: 4
      t.decimal :price, precision: 20, scale: 4
      t.decimal :invoiced_value, precision: 20, scale: 4
      t.integer :status, limit: 1
      t.timestamps null: false
    end

    create_table :order_confirmation do |t|
      t.references :company, index: true
      t.references :order, index: true
      t.references :administrator, index: true
      t.text :comments, limit: 65536
      t.timestamps null: false
    end

    create_table :order_refund do |t|
      t.references :company, index: true
      t.references :order, index: true
      t.references :administrator, index: true
      t.decimal :value, precision: 20, scale: 4
      t.text :comments, limit: 65536
      t.timestamps null: false
    end

    create_table :order_canceled do |t|
      t.references :company, index: true
      t.references :order, index: true
      t.references :administrator, index: true
      t.boolean :system, default: false
      t.text :comments, limit: 65536
      t.timestamps null: false
    end
    
    create_table :language do |t|
      t.string :name, limit: 80
      t.string :abbreviation, limit: 10
      t.timestamps null: false
    end
    
    create_table :access do |t|
      t.references :company, index: true
      t.references :user, index: true
      t.references :related, polymorphic: true, index: true
      t.timestamps null: false
    end
    
    create_table :photo do |t|
      t.references :company, index: true
      t.references :related, polymorphic: true, index: true
      t.string :code, limit: 80
      t.attachment :image
      t.integer :order, limit: 8
      t.boolean :enabled
      t.timestamps null: false
    end
    
    create_table :shared do |t|
      t.references :company, index: true
      t.references :user, index: true
      t.references :related, polymorphic: true, index: true
      t.string :token, limit: 80
      t.timestamps null: false
    end

    create_table :shared_access do |t|
      t.references :company, index: true
      t.references :shared, index: true
      t.string :device, limit: 80
      t.string :ip, limit: 80
      t.decimal :value, precision: 20, scale: 4
      t.timestamps null: false
    end

    create_table :connection do |t|
      t.references :company, index: true
      t.references :related, polymorphic: true, index: true
      t.string :device, limit: 80
      t.string :token, limit: 80
      t.timestamps null: false
    end
    
    create_table :translation do |t|
      t.references :company, index: true
      t.references :language, index: true
      t.references :related, polymorphic: true, index: true
      t.string :code, limit: 80
      t.string :value_string, limit: 200
      t.text :value_text, limit: 65536
      t.decimal :value_number, precision: 20, scale: 4
      t.datetime :value_datetime
      t.boolean :value_boolean
      t.attachment :image
      t.timestamps null: false
    end

  end
end